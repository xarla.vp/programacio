package senseif;

import java.util.Scanner;
import java.util.Locale;

public class QuadratAmbFormat {

	public static void main(String[] args) {
		// TODO Auto-generated method stub	
		Scanner src = new Scanner(System.in);
		
		double costat;
		double area;
		
		costat = src.nextDouble();
		area = costat * costat;
		
		System.out.printf(Locale.US,"%015.3f\n", area);
		
		
	}

}