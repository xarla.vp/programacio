package senseif;

import java.util.Scanner;

public class SumaUnSegon {

    public static void main(String[] args) {
        
        int hores;
        int minuts;
        int segons;
        
        Scanner src = new Scanner(System.in);

        hores = src.nextInt();
        minuts = src.nextInt();
        segons = src.nextInt();

        segons = segons + 1;

        if (segons == 60) {
            segons = 0;
            minuts = minuts + 1;
        }
        if (minuts == 60) {
            minuts = 0;
            hores = hores + 1;
        }
        if (hores == 24) {
            hores = 0;
        }

        System.out.printf(hores + " " + minuts + " " + segons);

        src.close();
    }
}