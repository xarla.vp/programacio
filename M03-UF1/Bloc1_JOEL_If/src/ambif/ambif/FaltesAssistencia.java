package ambif;

import java.util.Scanner;
import java.lang.Math;

public class FaltesAssistencia {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Scanner src = new Scanner(System.in);
		
		String tot = src.nextLine();
		
		String[] items = tot.split(" ");
		
		float hores = Integer.parseInt(items[0]);
		float just = Integer.parseInt(items[1]);
		float injust = Integer.parseInt(items[2]);
		
		if ((just > (hores / 100) * 20) || (injust > (hores / 100) * 10 )) {
			System.out.println("NO");
		}
		
		else if ((just > (hores / 100) * 20) && (injust > (hores / 100) * 10 )) {
			System.out.println("NO");
		}
		
		else {
			System.out.println("SI");
		}

} }