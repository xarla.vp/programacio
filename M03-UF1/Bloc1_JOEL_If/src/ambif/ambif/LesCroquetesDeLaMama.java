package ambif;

import java.util.Scanner;
import java.lang.Math;

public class LesCroquetesDeLaMama { // C R O Q U E T E S 

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Scanner src = new Scanner(System.in);
		
		int masa = src.nextInt();
		int grans = masa / 4;
		int petites = (int) (masa / 2.5) ;
		float sobrantg = masa % 4;
		float sobrantp = (float) (masa % 2.5);
		
		if (sobrantg < sobrantp) {
			System.out.print(grans + " croquetes grans\n");
			
			if (grans <= 9) {
				System.out.print("paella petita\n");
				System.out.print(sobrantg);
			}
			
			else if (grans > 9 && grans <= 18) {
				System.out.print("paella mitjana\n");
				System.out.print(sobrantg);
			}
			
			else if (grans > 18 && grans <= 36) {
				System.out.print("paella gran\n");
				System.out.print(sobrantg);
			}
			
			else if (grans > 36) {
				System.out.print("paella gegant\n");
				System.out.print(sobrantg);
			}
			
		}
		
		else if (sobrantg > sobrantp) {
			System.out.print(petites + " croquetes petites\n");
			
			if (petites <= 9) {
				System.out.print("paella petita\n");
				System.out.print(sobrantp);
			}
			
			else if (petites > 9 && petites <= 18) {
				System.out.print("paella mitjana\n");
				System.out.print(sobrantp);
			}
			
			else if (petites > 18 && petites <= 36) {
				System.out.print("paella gran\n");
				System.out.print(sobrantp);
			}
			
			else if (petites > 36) {
				System.out.print("paella gegant\n");
				System.out.print(sobrantp);
			}
		}
		
		else if (sobrantp == sobrantg){
			System.out.print( grans + " croquetes grans\n" );
			if (grans <= 9) {
				System.out.print("paella petita\n");
				System.out.print(sobrantg);
			}
			
			else if (grans > 9 && grans <= 18) {
				System.out.print("paella mitjana\n");
				System.out.print(sobrantg);
			}
			
			else if (grans > 18 && grans <= 36) {
				System.out.print("paella gran\n");
				System.out.print(sobrantg);
			}

			else if (grans > 36) {
				System.out.print( "paella gegant\n");
				System.out.print(sobrantg);
			}
		}

} }