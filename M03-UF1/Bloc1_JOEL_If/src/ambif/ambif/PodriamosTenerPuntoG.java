package ambif;

import java.util.Scanner;

public class PodriamosTenerPuntoG {

    public static void main(String[] args) {
        Scanner src = new Scanner(System.in);

        String n1 = src.nextLine();
        String n2 = src.nextLine();
        String n3 = src.nextLine();
        String n4 = src.nextLine();
        String n5 = src.nextLine();

        int contadorG = 0;

        if (n1.equals("G")) {
            contadorG = contadorG + 1;
        }
        if (n2.equals("G")) {
        	contadorG = contadorG + 1;
        }
        if (n3.equals("G")) {
        	contadorG = contadorG + 1;
        }
        if (n4.equals("G")) {
        	contadorG = contadorG + 1;
        }
        if (n5.equals("G")) {
        	contadorG = contadorG + 1;
        }

        if (contadorG == 0) {
            System.out.print("NO");
        } 
        
        else if (contadorG == 1) {
            System.out.print("SI");
        } 
        
        else if (contadorG > 1) {
            System.out.print("PUNTOS");
        }
        
    }
}