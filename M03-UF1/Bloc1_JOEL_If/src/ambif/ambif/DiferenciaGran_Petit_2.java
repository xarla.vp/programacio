package ambif;

import java.util.Scanner;
import java.lang.Math;

public class DiferenciaGran_Petit_2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Scanner src = new Scanner(System.in);
		
		int var1;
		int var2;
		int var3;
		
		var1 = src.nextInt();
		var2 = src.nextInt();
		var3 = src.nextInt();
		
		if (var1 > var2 && var2 > var3) {
			System.out.println(var1 - var3);
		}
		
		else if (var1 > var3 && var3 > var2) {
			System.out.println(var1 - var2);
		}
		
		else if (var2 > var3 && var3 > var1) {
			System.out.println(var2 - var1);
		}
		
		else if (var2 > var1 && var1 > var3) {
			System.out.println(var2 - var3);
		}
		
		else if (var3 > var1 && var1 > var2) {
			System.out.println(var3 - var2);
		}
		
		else if (var3 > var2 && var2 > var1) {
			System.out.println(var3 - var1);
		}
		
		else if (var1 > var2 && var2 == var3){
			System.out.println(var1 - var2);
		}
		
		else if (var2 > var3 && var1 == var3){
			System.out.println(var2 - var3);
		}
		
		else if (var3 > var2 && var2 == var1){
			System.out.println(var3 - var2);
		}
		
		else {
			System.out.println(0);
		}
		
} }