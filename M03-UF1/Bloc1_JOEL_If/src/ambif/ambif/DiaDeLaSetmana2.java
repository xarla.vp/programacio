package ambif;

import java.util.Scanner;
import java.lang.Math;

public class DiaDeLaSetmana2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Scanner src = new Scanner(System.in);
		
		int var1;
		String var2;
		
		var1 = src.nextInt();
		
		var2 = "";
		
		if (var1 % 7 <= 7) {
			var1 = var1 % 7;
		}
		
		else if (var1 % 7 > 7) {
			var1 = (var1 % 7) - 7;
		}
		
		
		if (var1 == 1) {
			var2 = "Monday";
		}
		
		else if (var1 == 2) {
			var2 = "Tuesday";
		}
		
		else if (var1 == 3) {
			var2 = "Wednesday";
		}
		
		else if (var1 == 4) {
			var2 = "Thursday";
		}
		
		else if (var1 == 5) {
			var2 = "Friday";
		}
		
		else if (var1 == 6) {
			var2 = "Saturday";
		}
		
		else if (var1 == 7) {
			var2 = "Sunday";
		}
		
		System.out.println(var2);
	}

}