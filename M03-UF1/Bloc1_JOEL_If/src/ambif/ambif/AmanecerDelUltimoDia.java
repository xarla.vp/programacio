package ambif;

import java.util.Scanner;

public class AmanecerDelUltimoDia {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Scanner src = new Scanner(System.in);

		int sec = src.nextInt();
		int horas = (sec / 3600);
		int dia = ((horas / 24) + 1);
		int horari = (horas % 24);
		String hora = "";

		if (horari < 12) {
			hora = "mati";
		}

		else {
			hora = "nit";
		}

		System.out.println(hora + " del dia " + dia);

	}

}