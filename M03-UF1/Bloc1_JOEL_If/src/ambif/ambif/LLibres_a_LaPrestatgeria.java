package ambif;

import java.util.Scanner;
import java.lang.Math;

public class LLibres_a_LaPrestatgeria {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Scanner src = new Scanner(System.in);
		
		int prestatges = src.nextInt();
		int capacitat = src.nextInt();
		int llibres = src.nextInt();
		
		int sobrant = llibres - (prestatges * capacitat);
		
		if (sobrant < 0) {
			System.out.print(0);
		}
		
		else {
			System.out.print(sobrant);
		}

} }