package ambif;

import java.util.Scanner;
import java.lang.Math;

public class CalcularLumens {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Scanner src = new Scanner(System.in);
		
		String var = src.nextLine();
		
		String[] items = var.split(",");
		
		String sala = items[0];
		String metres = items[1];
		int m = Integer.parseInt(metres);
		
		int num1 = 0;
		int num2 = 0;
		
		if ((!sala.equals("passadis") && !sala.equals("salo") && !sala.equals("cuina") && !sala.equals("menjador") && !sala.equals("lavabo") && !sala.equals("dormitori")) && (m <= 0)){
			System.out.print("ERROR: Habitacio i mides no reconegudes");
		}
		
		else if (!sala.equals("passadis") && !sala.equals("salo") && !sala.equals("cuina") && !sala.equals("menjador") && !sala.equals("lavabo") && !sala.equals("dormitori")) {
			System.out.print("ERROR: Habitacio no reconeguda");
		}
		else if (m <= 0) {
			System.out.print("ERROR: Mida incorrecta");
		}
		
		else if (sala.equals("salo")) {
			num1 = 100 * m;
			num2 = 250 * m;
			System.out.print("La quantitat de lumens per el " + sala + " es de " + num1 + " a " + num2 + " lumens");
		}
		
		else if (sala.equals("menjador")) {
			num1 = 350 * m;
			num2 = 500 * m;
			System.out.print("La quantitat de lumens per el " + sala + " es de " + num1 + " a " + num2 + " lumens");
		}
		
		else if (sala.equals("dormitori")) {
			num1 = 50 * m;
			num2 = 150 * m;
			System.out.print("La quantitat de lumens per el " + sala + " es de " + num1 + " a " + num2 + " lumens");
		}
		
		else if (sala.equals("cuina")) {
			num1 = 200 * m;
			num2 = 300 * m;
			System.out.print("La quantitat de lumens per la " + sala + " es de " + num1 + " a " + num2 + " lumens");
		}
		
		else if (sala.equals("lavabo")) {
			num1 = 150 * m;
			num2 = 200 * m;
			System.out.print("La quantitat de lumens per el " + sala + " es de " + num1 + " a " + num2 + " lumens");
		}
		
		else if (sala.equals("passadis")) {
			num1 = 100 * m;
			num2 = 200 * m;
			System.out.print("La quantitat de lumens per el " + sala + " es de " + num1 + " a " + num2 + " lumens");
		}
	
		
} }