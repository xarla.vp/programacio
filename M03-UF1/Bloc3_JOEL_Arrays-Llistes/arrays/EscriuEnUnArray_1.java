package arrays;

import java.util.Scanner;

public class EscriuEnUnArray_1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Scanner src = new Scanner(System.in);
		
		final int K = src.nextInt();
		
		int[] vec;
		vec = new int[K];
		
		int i = 0;
		
		while (i < K) {
			vec[i] = src.nextInt();
			i++;
			
		}
		
		int pos = src.nextInt();
		
		i = 0;
		
		while (i < K) {
			System.out.print(vec[i] + " ");
			i++;
			
		}
		
		System.out.print("\n" + vec[pos]);
		
	}

}
