package arrays;

import java.util.ArrayList;
import java.util.Scanner;

public class MultiplicahoTot {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner src = new Scanner(System.in);
		ArrayList<Integer> lista = new ArrayList<Integer>();
        
		int casos = src.nextInt();
		src.nextLine();

		while (casos > 0) {

			int nombres = src.nextInt();
			int pos = nombres;
			lista.clear();
			while (nombres > 0) {
				int num = src.nextInt();
				lista.add(num);
				nombres--;
			}
			nombres = pos;
			pos = 0;

			int mult = src.nextInt();

			while (pos < nombres) {
				int result = mult * lista.get(pos);
				System.out.println(result);
				pos++;
			}

			casos--;
		}
	}

}