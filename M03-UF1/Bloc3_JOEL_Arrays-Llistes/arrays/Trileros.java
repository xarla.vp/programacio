package arrays;
import java.util.ArrayList;
import java.util.Scanner;

public class Trileros {

    public static void main(String[] args) {
        Scanner src = new Scanner(System.in);

        int k = src.nextInt();
        src.nextLine();

        ArrayList<String> lista = new ArrayList<String>();

        for (int pos = 0; pos < k; pos++) {
            lista.add("0");
        }
        
        lista.set(0, "1");

        int num1 = 0;
        int num2 = 0;

        while (num1 != -1 && num2 != -1) {
            num1 = src.nextInt();
            num2 = src.nextInt();

            if (num1 == -1 && num2 == -1) {
                break;
            }

            if (num1 < lista.size() && num2 < lista.size()) {
                String antNum1 = lista.get(num1);
                lista.set(num1, lista.get(num2));
                lista.set(num2, antNum1);
            }
        }

        int posicioBola = 0;
        for (int pos = 0; pos < lista.size(); pos++) {
            if (lista.get(pos).equals("1")) {
                posicioBola = pos;
                break;
            }
        }

        System.out.println(lista);
        System.out.println(posicioBola);

        src.close();
    }
}
