package arrays;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class LaFormulaEuler {

    public static void main(String[] args) {

        Scanner src = new Scanner(System.in);

        for (int casos = src.nextInt(); casos > 0; casos--) {
            int k = src.nextInt();
            src.nextLine();
            
            System.out.println();
            ArrayList<Integer> lista = new ArrayList<Integer>();
            
            for (int pos = 0; pos < k; pos++) {
                int num = src.nextInt();
                lista.add(num);
            }
            
            Collections.sort(lista);
            
            while (lista.size() > 0) {
            	
            	int suma = lista.get(0) + lista.get(lista.size() - 1);
            	
            	System.out.print(suma + " ");
                
                lista.remove(lista.size() - 1);
                lista.remove(0);
                
            }
        }
    }
}
