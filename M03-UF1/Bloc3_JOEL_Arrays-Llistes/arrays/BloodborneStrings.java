package arrays;

import java.util.ArrayList;
import java.util.Scanner;

public class BloodborneStrings {

    public static void main(String[] args) {

        Scanner src = new Scanner(System.in);

        int casos = src.nextInt();
        src.nextLine();

        for (int i = 0; i < casos; i++) {
        	
            String paraula = src.nextLine();
            int cont = 0;
            int k = paraula.length();

            for (int pos = 1; pos < k; pos++) {
            	
                if (paraula.charAt(pos) == paraula.charAt(pos - 1)) {
                    cont++;
                    break;
                    
                }
            }

            if (cont > 0) {
                System.out.println("SI");
            } 
            
            else {
                System.out.println("NO");
            }
            
        }
        
    }
    
}

