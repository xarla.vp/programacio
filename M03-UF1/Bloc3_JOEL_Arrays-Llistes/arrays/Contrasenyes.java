package arrays;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class Contrasenyes {

	public static void main(String[] args) {

		Scanner src = new Scanner(System.in);

		int k = src.nextInt();
		src.nextLine();

		ArrayList<String> users = new ArrayList<String>();
		ArrayList<String> contr = new ArrayList<String>();

		for (int pos = 0; pos < k; pos++) {
			String user = src.next();
			users.add(user);
		}

		for (int pos = 0; pos < k; pos++) {
			String pass = src.next();
			contr.add(pass);
		}

		int N = src.nextInt();
		src.nextLine();

		for (int pos = 0; pos < N; pos++) {

			String loginuser = src.next();
			String loginpass = src.next();

			if (!users.contains(loginuser)) {
				System.out.println("usuari no trobat");
			} 
			
			else {
				
				if (contr.get(pos).equals(loginpass)) {
					System.out.println("OK");
				} 
				
				else {
					System.out.println("contrasenya incorrecta");
				}
			}
		}

		Collections.sort(users);
		
		for (int pos = 0; pos < k; pos++) {
			System.out.print(users.get(pos) + " ");
		}
		
	}
}
