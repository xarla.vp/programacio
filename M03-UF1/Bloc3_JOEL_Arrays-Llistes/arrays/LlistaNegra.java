package arrays;

import java.util.ArrayList;
import java.util.Scanner;

public class LlistaNegra {

    public static void main(String[] args) {

        Scanner src = new Scanner(System.in);

        for (int casos = src.nextInt(); casos > 0; casos--) {
            src.nextLine();

            ArrayList<String> listanoms = new ArrayList<String>();
            listanoms.clear();
            ArrayList<Integer> listanums = new ArrayList<Integer>();
            listanums.clear();

            int nivellShitlist = 0;

            String noms = src.nextLine();

            String[] nom = noms.split(" ");

            int k = nom.length;

            for (int pos = 0; pos < k; pos++) {
                listanoms.add(nom[pos]);
            }

            int n = src.nextInt();

            for (int pos = 0; pos < n; pos++) {
                listanums.add(src.nextInt());
            }

            for (int pos = 0; pos < listanums.size(); pos++) {
            	
                int hora = listanums.get(pos);
                String act = listanoms.get(hora - 15);

                if (!act.equals("Eclipse")) {
                	
                    if (act.equals("LoL")) {
                    	
                        nivellShitlist = 2;
                        break;
                        
                    } 
                    
                    else if (act.equals("HollowKnight") || act.equals("DarkSouls") || act.equals("Zelda")) {
                        nivellShitlist = 1;
                    }
                }
            }

            System.out.println("Nivell de Shitlist: " + nivellShitlist);
        }

    }
    
}
