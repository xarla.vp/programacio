package arrays;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class EscalaReial {

    public static void main(String[] args) {

        Scanner src = new Scanner(System.in);

        for (int casos = src.nextInt(); casos > 0; casos--) {
            int k = 7;
            src.nextLine();
            ArrayList<Integer> lista = new ArrayList<Integer>();
            
            for (int pos = 0; pos < k; pos++) {
                int num = src.nextInt();
                lista.add(num);
            }
            
            Collections.sort(lista);
            
            if (lista.contains(11)
            		&& lista.contains(12)
            		&& lista.contains(13)
            		&& lista.contains(1)) {
        		
        		System.out.println("ESCALA REIAL");
        		
            }
            
            else {
	            for (int pos = 0; pos < k; pos++) {
	            	
	            	if (lista.contains(lista.get(pos) + 1)
	                		&& lista.contains(lista.get(pos) + 2)
	                		&& lista.contains(lista.get(pos) + 3)
	                		&& lista.contains(lista.get(pos) + 4)) {
	            		
	            		System.out.println("ESCALA");
	            		break;
	            		
	                }
	            	
	            	else {
	            		System.out.println("NO");
	            		break;
	            	}
	            }
            }
        }
    }
}
