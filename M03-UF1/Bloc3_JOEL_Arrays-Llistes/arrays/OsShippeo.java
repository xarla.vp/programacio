package arrays;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class OsShippeo {

    public static void main(String[] args) {
        Scanner src = new Scanner(System.in);

        int casos = src.nextInt();
        src.nextLine();

        while (casos > 0) {

            int k = src.nextInt();

            ArrayList<Integer> lista = new ArrayList<>();

            int pos = 0;

            while (pos < k) {
            	int num = src.nextInt();
            	lista.add(num);
                pos++;
            }
            
            Collections.sort(lista);
            int max = lista.get(lista.size() - 1);
            int min = lista.get(0);

            int dif = max - min;

            System.out.println(dif);

            casos--;
        }

        src.close();
    }
}

