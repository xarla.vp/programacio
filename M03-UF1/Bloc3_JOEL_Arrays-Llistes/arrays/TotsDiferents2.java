package arrays;

import java.util.ArrayList;
import java.util.Scanner;

public class TotsDiferents2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Scanner src = new Scanner(System.in);

		int casos = src.nextInt();
		src.nextLine();
		
		ArrayList<Integer> lista1 = new ArrayList<>();
		ArrayList<Integer> lista2 = new ArrayList<>();


		while (casos > 0) {
			
			int k = src.nextInt();
			src.nextLine();
			
			lista1.clear();
			lista2.clear();
			
			int pos = 0; 
			
			int repes = 0;
			
			while (pos < k) {
				
				int nom = src.nextInt();
				lista1.add(nom);
				
			    pos++;
			}
			
			pos = 0;
			
			while (pos < k) {
				
				int nom = src.nextInt();
				lista2.add(nom);
				
			    pos++;
			}
			
			pos = 0;
			
			while (pos < k) {
				
				if (lista1.contains(lista2.get(pos))) { // solo he cambiado esta linea del anterior
					repes++;
				}
				
			    pos++;
			}
			
			System.out.println();
			
			if (repes > 0) {
				System.out.print("NO");
			}
			
			else {
				System.out.print("SI");
			}
			
			casos--;
		}

	}
}