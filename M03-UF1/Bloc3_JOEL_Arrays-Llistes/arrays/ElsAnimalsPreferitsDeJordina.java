package arrays;

import java.util.ArrayList;
import java.util.Scanner;

public class ElsAnimalsPreferitsDeJordina {

    public static void main(String[] args) {

        Scanner src = new Scanner(System.in);

        for (int casos = src.nextInt(); casos > 0; casos--) {
        	
            int k = src.nextInt();
            
            src.nextLine();
            
            ArrayList<String> lista = new ArrayList<String>();
            
            lista.clear();
            
            String nom = " ";
            int cont = 0;
            
            for (int pos = 0; pos < k; pos++) {
                nom = src.nextLine();
                if (lista.contains(nom)) {
                	cont++;
                }
                lista.add(nom);
            }
            
            if (cont > 0) {
            	System.out.println("SI");
            }
            
            else {
            	System.out.println("NO");
            }
            
        }
        
    }
    
}
