package arrays;

import java.util.ArrayList;
import java.util.Scanner;

public class TotsMoltDreta {

    public static void main(String[] args) {

        Scanner src = new Scanner(System.in);

        for (int casos = src.nextInt(); casos > 0; casos--) {
            int k = src.nextInt();
            src.nextLine();
            ArrayList<Integer> lista = new ArrayList<Integer>();
            
            for (int pos = 0; pos < k; pos++) {
                int num = src.nextInt();
                lista.add(num);
                
            }
            
            int mesura = k - 1;      
            
            int n = src.nextInt();
            
            for (int pos = 0; pos < n; pos++) {
            	int ultim = lista.remove(mesura);
            	lista.add(0, ultim);
        		
            }
            
            
            for (int pos = 0; pos < k; pos++) {
            	System.out.print(lista.get(pos) + " ");
        		
            }
            
        }
    }
}
