package arrays;

import java.util.Scanner;

public class ComptarEnArrays {

    public static void main(String[] args) {
    	// TODO Auto-generated method stub
    	
        Scanner src = new Scanner(System.in);

        int casos = src.nextInt();
        int cont = 0;
        int pos = 0;

        while (casos > 0) {
        	final int K = src.nextInt(); 

            int[] vec = new int[K];
            
            while (pos < K) {
            	vec[pos] = src.nextInt();
            	
            	pos++;
            }
            
            pos = 0;
            
            int P = src.nextInt();
            
            while (pos < K) {
            	
            	if (vec[pos] == P) {
            		cont++;
            	}
            	
            	pos++;
            	
            }
            
            System.out.print("\n");
            System.out.print(cont);
            
            cont = 0;
            pos = 0;
            
            casos--;
            
        }
    }
}