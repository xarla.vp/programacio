package arrays;

import java.util.ArrayList;
import java.util.Scanner;

public class Matricula {

    public static void main(String[] args) {

        Scanner src = new Scanner(System.in);
        
        String entrada = src.nextLine();
        
        ArrayList<String> sumletras = new ArrayList<String>();
        
        	// -----------------------------
        
	        for (int pos = 0; pos < 26; pos++) {
	        	
	            char letra = (char) ('A' + pos);
	
	            if (letra != 'A' && letra != 'E' && letra != 'I' && letra != 'O' && letra != 'U' && letra != 'Ñ') {
	                sumletras.add("" + letra);
	            }
	            
	        }
	        
	        // -----------------------------
        
	        while (!entrada.equals("9999 ZZZ")) {

            String[] part = entrada.split(" ");
            
            int num = Integer.parseInt(part[0]);
            
            // -----------------------------
            
            char letra1 = part[1].charAt(0);
            char letra2 = part[1].charAt(1);
            char letra3 = part[1].charAt(2);
            
            // -----------------------------
            
            if (num == 9999) {
                num = 0;
               
                if (letra3 != 'Z' && !sumletras.contains((char) (letra3 + 1))) {
                    	letra3++;
                    	
                    	if (!sumletras.contains(letra3)) {
                        	letra3++;
                        }
                } 
                
                else if (letra2 != 'Z' && !sumletras.contains((char) (letra2 + 1))) {
                    letra2++;
                	letra3 = 'B';
                	
                	if (!sumletras.contains(letra2)) {
                    	letra2++;
                    }
                } 
                
                else if (letra1 != 'Z' && !sumletras.contains((char) (letra1 + 1))) {
                	
                    letra1++;
                    letra2 = 'B';
                    letra3 = 'B';
                    
                    if (!sumletras.contains(letra1)) {
                    	letra1++;
                    }
                } 
                
                else {
                    letra1 = 'B';
                    letra2 = 'B';
                    letra3 = 'B';
                }
                
            } 
            
            else {
                num++;
            }
            
            // -----------------------------
            
            System.out.println(String.format("%04d %c%c%c", num, letra1, letra2, letra3));
            
            // -----------------------------
            
            entrada = src.nextLine();
            
        }
    }
}