package arrays;

import java.util.Scanner;

public class EscriuEnUnArray_2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Scanner src = new Scanner(System.in);
		
		final int K = src.nextInt();
		
		src.nextLine();
		
		String[] vec;
		vec = new String[K];
		
		int i = 0;
		
		while (i < K) {
			vec[i] = src.nextLine();
			i++;
			
		}
		
		int pos = src.nextInt();
		
		i = 0;
		
		while (i < K) {
			System.out.print("\n");
			System.out.print(vec[i]);
			i++;
			
		}
		
		System.out.print("\n" + vec[pos]);
		
	}

}
