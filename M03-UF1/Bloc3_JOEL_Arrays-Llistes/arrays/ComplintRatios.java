package arrays;

import java.util.ArrayList;
import java.util.Scanner;

public class ComplintRatios {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Scanner src = new Scanner(System.in);

		int casos = src.nextInt();
		src.nextLine();

		while (casos > 0) {
			
			int k = src.nextInt();
			src.nextLine();

			ArrayList<String> llistanoms = new ArrayList<>();
			
			String entrada = src.nextLine();
			
			String[] noms = entrada.split(" ");
			
			int pos = 0; 
			
			while (pos < k) {
				llistanoms.add(noms[pos]);
			    pos++;
			}

			int p = src.nextInt();
			src.nextLine();
			
			llistanoms.remove(p);

			pos = 0;
			
			while (pos < llistanoms.size()) {
			    String nom = llistanoms.get(pos);
			    System.out.print(nom + " ");
			    pos++;
			}
			
			System.out.println();
			
			casos--;
		}

	}
}