package arrays;

import java.util.ArrayList;
import java.util.Scanner;

public class EscriuEnUnArray_3 {

    public static void main(String[] args) {
    	// TODO Auto-generated method stub
    	
        Scanner src = new Scanner(System.in);
        
        ArrayList lista = new ArrayList<Integer>();
        
        int entrada = src.nextInt();
        
        while (entrada != - 1) {
        	lista.add(entrada);
        	entrada = src.nextInt();
        }
        
        int pos = src.nextInt();
        
        System.out.print(lista);
        
        System.out.print("\n");
        
        System.out.print(lista.get(pos));
        
    }
}