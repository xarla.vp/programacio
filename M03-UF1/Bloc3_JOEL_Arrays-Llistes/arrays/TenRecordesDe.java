package arrays;

import java.util.Scanner;

public class TenRecordesDe {
	
    public static void main(String[] args) {
    	// TODO Auto-generated method stub
    	
        Scanner src = new Scanner(System.in);

        int casos = src.nextInt();

        while (casos > 0) {
        	final int K = src.nextInt(); 
        	// K = cantidad de números en el array

            int[] vec = new int[K];
            
            // Creamos el array y le decimos cuantas cosas van dentro
            
            int pos = 0;
            
            // Contador de posición para el array
            
            while (pos < K) {
            // si la posición es nenor que la cantidad total de números
            	vec[pos] = src.nextInt();
            // pedimos al usuario que llene el hueco (pos) del array
            	pos++;
            // siguiente posición
            }
            
            // pedimos al usuario que nos de la posición que quiere ver
            int P = src.nextInt();
            
            // imprimimos la posición (P) de los números que hay dentro del array
            System.out.println(vec[P]);

            casos--;
            
        }
    }
}
