package arrays;

import java.util.Scanner;

public class Padel {

    public static void main(String[] args) {

        Scanner src = new Scanner(System.in);

        boolean finCat = false;

        while (!finCat) {

            String categoria = src.nextLine();
            String ganador = "";

            if (categoria.equals("FIN")) {
                finCat = true;
            } else {

                int puntos = 0;
                int nojugados = 0;

                boolean fin = false;

                while (!fin) {

                    String resultados = src.nextLine();

                    if (resultados.equals("FIN")) {
                        fin = true;
                    } 
                    
                    else {
                        String[] resultado = resultados.split(" ");

                        if (resultado.length >= 4) {
                            String equipoL = resultado[0];
                            int setsL = Integer.parseInt(resultado[1]);

                            String equipoV = resultado[2];
                            int setsV = Integer.parseInt(resultado[3]);

                            if (setsL > setsV) {
                                puntos += 2;
                                ganador = equipoL;
                            } 
                            
                            else if (setsV > setsL) {
                                puntos += 1;
                                ganador = equipoV;
                            }

                            if (setsL == 0 || setsV == 0) {
                                nojugados++;
                                puntos = 0;
                            }
                        }
                    }
                }

                if (nojugados / 3 != 0) {
                    System.out.println(ganador + " " + nojugados / 3);
                } 
                
                else if (nojugados / 3 == 0) {
                    System.out.println("EMPATE " + nojugados / 3);
                }
            }
        }
    }
}
