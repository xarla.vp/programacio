package arrays;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class OrdenaAlfabeticamente {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Scanner src = new Scanner(System.in);
		
		int casos = src.nextInt();
		src.nextLine();
		
		ArrayList listaletras = new ArrayList<String>();
		
		while (casos > 0) {
			
			listaletras.clear();
			
			String letras = src.nextLine();
			
			int pos = 0;
			
			while (pos < letras.length()) {
				char letra = letras.charAt(pos);
				listaletras.add(letra);
				pos++;
				
			}
			
			Collections.sort(listaletras);
			
			pos = 0;
			
			System.out.print("\n");
            while (pos < listaletras.size()) {
            	if (!listaletras.get(pos).equals(" ")) {
                	System.out.print(listaletras.get(pos));
            	}
                pos++;
            }
			
			casos--;
			
		}
		
		
	}

}
