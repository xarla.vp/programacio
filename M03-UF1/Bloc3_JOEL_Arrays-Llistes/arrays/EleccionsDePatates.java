package arrays;

import java.util.ArrayList;
import java.util.Scanner;

public class EleccionsDePatates {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Scanner src = new Scanner(System.in);
        
        for (int casos = src.nextInt(); casos > 0; casos--) {
            int k = src.nextInt();
            ArrayList<Integer> lista = new ArrayList<Integer>();
            lista.clear();
            int maxpos = 0;
            int maxnum = 0;
            
            for (int pos = 0; pos < k; pos++) {
                int num = src.nextInt();
                lista.add(num);
            }
            
            for (int pos = 0; pos < k; pos++) {
                if (lista.get(pos) > maxnum) {
                	maxnum = lista.get(pos);
                	maxpos = pos + 1;
                }
            }
            
            System.out.print(maxpos);
            System.out.println();
            
            }

            
     }

}