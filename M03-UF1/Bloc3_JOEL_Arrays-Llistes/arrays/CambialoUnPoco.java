package arrays;

import java.util.Scanner;

public class CambialoUnPoco {

    public static void main(String[] args) {
        Scanner src = new Scanner(System.in);
        
        for (int casos = src.nextInt(); casos > 0; casos--) {
            int k = src.nextInt();
            int[] array = new int[k];
            
            for (int pos = 0; pos < k; pos++) {
                array[pos] = src.nextInt();
            }

            int v1 = src.nextInt();
            int v2 = src.nextInt();
            
            for (int pos = 0; pos < k; pos++) {
                if (array[pos] == v1) {
                    array[pos] = v2;
                }
            }
            
            for (int pos = 0; pos < k; pos++) {
                System.out.print(array[pos]);

                if (pos < k - 1) {
                    System.out.print(" ");
                }
            }

            System.out.println();
        }

    }
}

