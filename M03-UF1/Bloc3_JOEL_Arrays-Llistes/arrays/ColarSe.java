package arrays;

import java.util.ArrayList;
import java.util.Scanner;

public class ColarSe {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Scanner src = new Scanner(System.in);
		
		ArrayList<Integer> lista = new ArrayList<Integer>();
        
		int casos = src.nextInt();
		src.nextLine();

		while (casos > 0) {

			int K = src.nextInt();
			
			lista.clear();
			
			while (K > 0) {
				int num = src.nextInt();
				lista.add(num);
				K--;
			}
			
			int C = src.nextInt();
			int P = src.nextInt();
			
			int pos = 0;
			
			lista.add(P,  C);
			
			System.out.println();
			
			while (lista.size() > pos) {
			System.out.print(lista.get(pos) + " ");
			pos++;
			}
			
			casos--;
		}
	}

}