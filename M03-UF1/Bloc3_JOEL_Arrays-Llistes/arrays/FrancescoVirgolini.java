package arrays;

import java.util.ArrayList;
import java.util.Scanner;

public class FrancescoVirgolini {

    public static void main(String[] args) {

        Scanner src = new Scanner(System.in);

        for (int casos = src.nextInt(); casos > 0; casos--) {
            int k = src.nextInt();
            src.nextLine();
            ArrayList<String> lista = new ArrayList<String>();

            for (int pos = 0; pos < k; pos++) {
                String nom = src.nextLine();
                lista.add(nom);
            }

            int posFrancesco = lista.indexOf("Francesco Virgolini");

            if (posFrancesco > 0) {
                String francesco = lista.remove(posFrancesco);
                lista.add(posFrancesco - 1, francesco);
            }

            System.out.println(lista);
        }
    }
}
