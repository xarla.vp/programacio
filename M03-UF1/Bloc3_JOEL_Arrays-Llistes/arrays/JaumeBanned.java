package arrays;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class JaumeBanned {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
			
		Scanner src = new Scanner(System.in);
		
		int casos = src.nextInt();
		src.nextLine();
		
		ArrayList listaban = new ArrayList<String>();
		
		listaban.add("hada");
		listaban.add("uwu");
		listaban.add("owo");
		listaban.add("sacapuntas");
		listaban.add("adolfito");
		listaban.add("35");
		
		int cont = 0;
		
		while (casos > 0) {
			
			String frase = src.nextLine();
			frase = frase.toLowerCase();
			
			String[] paraules = frase.split(" ");
			
			int pos = 0;
			
			while (pos < paraules.length) {
				
				String paraula = paraules[pos];
				
				if (listaban.contains(paraula)) {
	                cont++;
	            } 
				
				pos++;
				
			}
			
			if (cont > 0) {
				System.out.print("\n");
				System.out.print("Jaime ha recibido un Ban");
			}
			
			else {
				System.out.print("\n");
				System.out.print("No Ban a Jaime");
			}
			
			cont = 0;
			casos--;
		}
		
	}

}
