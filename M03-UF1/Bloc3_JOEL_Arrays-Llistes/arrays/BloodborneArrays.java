package arrays;

import java.util.ArrayList;
import java.util.Scanner;

public class BloodborneArrays {

    public static void main(String[] args) {

        Scanner src = new Scanner(System.in);

        for (int casos = src.nextInt(); casos > 0; casos--) {
            int k = src.nextInt();
            src.nextLine();
            ArrayList<Integer> lista = new ArrayList<Integer>();
            int ant = - 1;
            int nom = - 1;
            int cont = 0;
            
            for (int pos = 0; pos < k; pos++) {
            	ant = nom;
                nom = src.nextInt();
                lista.add(nom);
                if (ant == nom) {
                	cont++;
                }
            }
            
            if (cont > 0) {
            	System.out.println("SI");
            }
            
            else {
            	System.out.println("NO");
            }
            
        }
    }
}
