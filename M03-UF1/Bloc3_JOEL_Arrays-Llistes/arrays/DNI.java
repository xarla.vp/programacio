package arrays;

import java.util.ArrayList;
import java.util.Scanner;

public class DNI {

    public static void main(String[] args) {

        Scanner src = new Scanner(System.in);

        int casos = src.nextInt();
        src.nextLine();
        
        String lletres = "TRWAGMYFPDXBNJZSQVHLCKE";

        for (int i = 0; i < casos; i++) {
            String dni = src.nextLine();

            String numerosDNI = dni.substring(0, 8);

            char lletra = dni.charAt(8);
            
            int numeros = Integer.parseInt(numerosDNI);

            if (numeros % 23 == lletres.indexOf(lletra)) {
                System.out.println("valid");
            } 
            
            else {
                System.out.println("invalid");
            }
            
            
        }
    }
}
