package arrays;

import java.util.Scanner;

public class MacarrismeAl100 {

    public static void main(String[] args) {
        Scanner src = new Scanner(System.in);

        int N = src.nextInt();
        src.nextLine();

        String tot = src.nextLine(); 
        String[] nums = tot.split(" ");

        int poscas = 0;
        
        while (poscas < N) {
            double num = Double.parseDouble(nums[poscas]);
            System.out.print((num * 100) + "% ");
            poscas++;
        }

    }
}

