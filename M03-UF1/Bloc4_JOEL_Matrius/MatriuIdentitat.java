package matrius;

import java.util.Scanner;

public class MatriuIdentitat {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Scanner src = new Scanner(System.in);
		
		int f = src.nextInt();
		int c = f;
		
		int[][] matriu = new int[f][c];
		
		for (int i = 0; i < f; i++) {
			for (int j = 0; j < c; j++) {
				if (i == j) {
					System.out.print("1 ");
				}
				
				else {
					System.out.print("0 ");
				}
			}
			System.out.println();
		}
		
	}

}
