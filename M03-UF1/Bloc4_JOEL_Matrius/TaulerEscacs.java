package matrius;

import java.util.Scanner;

public class TaulerEscacs {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Scanner src = new Scanner(System.in);
		
		int f = src.nextInt();
		int c = src.nextInt();
		
		String[][] matriu = new String[f][c];
		
		for (int i = 0; i < f; i++) {
			for (int j = 0; j < c; j++) {
				
				if ( i % 2 == 0) {
					if (j % 2 == 0) {
						matriu[i][j] = ".";
					}
					
					else {
						matriu[i][j] = "#";
					}
				}
				
				else {
					if (j % 2 != 0) {
						matriu[i][j] = ".";
					}
					
					else {
						matriu[i][j] = "#";
					}
				}
	        }
        }
		
		for (int i = 0; i < f; i++) {
			for (int j = 0; j < c; j++) {
				System.out.print(matriu[i][j] + " ");
	        }
				System.out.println();
        }
		
	}

}
