package matrius;

import java.util.Scanner;

public class ElCuadradoDeRubik {

    public static void main(String[] args) {
        Scanner src = new Scanner(System.in);

        int entrada = src.nextInt();

        while (entrada != 0) {
            char[][] matriu = new char[entrada][entrada];

            src.nextLine();

            for (int i = 0; i < entrada; i++) {
                String fila = src.nextLine();

                for (int j = 0; j < entrada; j++) {
                    matriu[i][j] = fila.charAt(j);
                }
            }
            
            
            /* ----------------------------------------- */
            	
            String giro = src.nextLine();
            
            while (!giro.equals("x")) {
            	
            	char fc = giro.charAt(0);
            	
            	char num = giro.charAt(1);
            	
            	if (fc == 'f') {
            	    if (num > 0) {
            	        char ant = matriu[num - 1][entrada - 1];

            	        for (int j = entrada - 1; j > 0; j--) {
            	            matriu[num - 1][j] = matriu[num - 1][j - 1];
            	        }

            	        matriu[num - 1][0] = ant;
            	    }
            		
            	    else {
            	        char ant = matriu[-num - 1][0];

            	        for (int j = 0; j < entrada - 1; j++) {
            	            matriu[-num - 1][j] = matriu[-num - 1][j + 1];
            	        }

            	        matriu[-num - 1][entrada - 1] = ant;
            	    }
            	}
            	
            	else {
            		
            		if (num > 0) {
            	        char ant = matriu[entrada - 1][num - 1];

            	        for (int i = entrada - 1; i > 0; i--) {
            	            matriu[i][num - 1] = matriu[i - 1][num - 1];
            	        }

            	        matriu[0][num - 1] = ant;
            	    }
            		
            	    else {
            	        char ant = matriu[0][-num - 1];

            	        for (int i = 0; i < entrada - 1; i++) {
            	            matriu[i][-num - 1] = matriu[i + 1][-num - 1];
            	        }

            	        matriu[entrada - 1][-num - 1] = ant;
            	    }
            		
            	}
            	
	            giro = src.nextLine();
	            
            }
            
            /* ----------------------------------------- */
            
            
            for (int i = 0; i < entrada; i++) {
                for (int j = 0; j < entrada; j++) {
                    System.out.print(matriu[i][j]);
                }
                System.out.println();
            }
            
            System.out.println("---");
            entrada = src.nextInt();
            
        }
    }
}