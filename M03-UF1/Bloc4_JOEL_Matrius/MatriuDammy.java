package matrius;

import java.util.Random;
import java.util.Scanner;

public class MatriuDammy {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Scanner src = new Scanner(System.in);
		
		Random rnd = new Random();
		
		int n = rnd.nextInt(9) + 6;
		
		int f = n;
		int c = n;
		
		char[][] matriu = new char[f][c];
		
		for (int i = 0; i < f; i++) {
			for (int j = 0; j < c; j++) {
				if (i == j) {
					matriu[i][j] = 'A';
				}
				
				else {
					matriu[i][j] = '0';
				}
				
	        }
			
        }
		
		for (int i = 1; i < f; i++) {
			for (int j = 1; j < c; j++) {
				if (i == j) {
					matriu[i - 1][j] = 'M';
				}
	        }
        }
		
		for (int i = 0; i < f - 1; i++) {
			for (int j = 0; j < c - 1; j++) {
				if (i == j) {
					matriu[i + 1][j] = 'D';
				}
	        }
        }
		
		for (int i = 0; i < f; i++) {
			for (int j = 0; j < c; j++) {
				System.out.print(matriu[i][j] + " ");
	        }
				System.out.println();
        }
		
	}

}
