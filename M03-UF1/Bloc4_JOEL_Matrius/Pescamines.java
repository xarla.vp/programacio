package matrius;

import java.util.Scanner;

public class Pescamines {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Scanner src = new Scanner(System.in);
		
		for (int casos = src.nextInt(); casos > 0; casos--) {
			int inutil = src.nextInt();
			int f = src.nextInt() +2;
            int c = src.nextInt() +2;

            int[][] matriu = new int[f][c];

            for (int i = 0; i < f; i++)
				for (int pos = 0; pos < c; pos++)
					if (pos == 0 || pos == c - 1 || i == 0 || i == f - 1) {
						matriu[i][pos] = 0;
					} else {
						matriu[i][pos] = src.nextInt();
					}


            int i = src.nextInt() +1;
            int j = src.nextInt() +1;

            if (matriu[i][j] == 1) {
                System.out.println("BOOM");
            } 
            
            else {
                int cont = 0;
                if (matriu[i + 1][j] == 1) {
                    cont++;
                }
                
                if (matriu[i + 1][j + 1] == 1) {
                    cont++;
                }
                
                if (matriu[i][j + 1] == 1) {
                    cont++;
                }
                
                if (i - 1 >= 0 && matriu[i - 1][j] == 1) {
                    cont++;
                }
                
                if (i - 1 >= 0 && matriu[i - 1][j - 1] == 1) {
                    cont++;
                }
                
                if (i - 1 >= 0 && matriu[i][j - 1] == 1) {
                    cont++;
                }
                
                if (matriu[i + 1][j - 1] == 1) {
                    cont++;
                }
                
                if (matriu[i - 1][j + 1] == 1) {
                    cont++;
                }
                
                System.out.println(cont);
                
            }
        }
    }
}