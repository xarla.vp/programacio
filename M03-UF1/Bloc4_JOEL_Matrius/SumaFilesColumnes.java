package matrius;

import java.util.Scanner;

public class SumaFilesColumnes {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Scanner src = new Scanner(System.in);
		
		int f = src.nextInt();
		int c = src.nextInt();
		
		int[][] matriu1 = new int[f][c];
		
		int sumaf = 0;
		int sumac = 0;
		
		for (int i = 0; i < f; i++) {
			for (int j = 0; j < c; j++) {
	            matriu1[i][j] = src.nextInt();
	        }
        }
		
		int k = src.nextInt();
		
		for (int j = 0; j < c; j++) {
            sumaf = sumaf + matriu1[k][j];
        }

        for (int i = 0; i < f; i++) {
            sumac = sumac + matriu1[i][k];
        }

        System.out.println(sumaf + " " + sumac);
		
	}

}
