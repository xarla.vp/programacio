package matrius;

import java.util.Scanner;

public class Notes_1 {

    public static void main(String[] args) {

        Scanner src = new Scanner(System.in);

        for (int casos = src.nextInt(); casos > 0; casos--) {
            int f = src.nextInt();
            int c = src.nextInt();

            int[][] matriu = new int[f][c];

            for (int i = 0; i < f; i++) {
                for (int j = 0; j < c; j++) {
                    matriu[i][j] = src.nextInt();
                }
            }
            
            for (int i = 0; i < f; i++) {
                int suma = 0;
                for (int j = 0; j < c; j++) {
                    suma += matriu[i][j];
                }
                int mitjana = suma / c;
                System.out.print(mitjana + " ");
            }
        }
    }
}
