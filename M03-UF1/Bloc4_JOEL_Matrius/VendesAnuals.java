package matrius;

import java.util.Scanner;

public class VendesAnuals {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int casos = scanner.nextInt();

        for (int i = 0; i < casos; i++) {
            int[] vendes = new int[12];

            for (int j = 0; j < 12; j++) {
                vendes[j] = scanner.nextInt();
            }

            int minimVendes = scanner.nextInt();

            boolean vendesHanAugmentat = true;
            int primerMesNegatiu = -1;

            for (int j = 1; j < 12; j++) {
                if (vendes[j] <= vendes[j - 1] || vendes[j] < minimVendes) {
                    vendesHanAugmentat = false;
                    if (primerMesNegatiu != vendes[j]) {
                    	primerMesNegatiu = j + 1; 
                        break;
                    }
                }
            }

            if (vendesHanAugmentat) {
                System.out.println("SI");
            } 
            
            else {
                System.out.println("NO " + primerMesNegatiu);
            }
        }
    }
}
