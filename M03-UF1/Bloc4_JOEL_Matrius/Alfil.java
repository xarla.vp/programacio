package matrius;

import java.util.Scanner;

public class Alfil {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner src = new Scanner(System.in);
		
		int casos = src.nextInt();
		src.nextLine();
		for (; casos > 0; casos --) {
			String pos = src.nextLine();
			int col = pos.charAt(0) - 'a';
			int fila = pos.charAt(1) - '0' - 1;
			
			int cont = 0;
			int i = fila + 1;
			int j = col + 1;
			while (i < 8 && j < 8) {
				cont++;
				i++;
				j++;
			}
			
			i = fila - 1;
			j = col - 1;
			while (i >= 0 && j >= 0) {
				cont++;
				i--;
				j--;
			}
			
			i = fila - 1;
			j = col + 1;
			while (i >= 0 && j < 8) {
				cont++;
				i--;
				j++;
			}
			
			i = fila + 1;
			j = col - 1;
			while (i < 8 && j >= 0) {
				cont++;
				i++;
				j--;
			}
			
			System.out.println(cont);
			
		}
		
	}

}
