package matrius;

import java.util.Scanner;

public class NatacioSincronitzada {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner src = new Scanner(System.in);
		
		for (int casos = src.nextInt(); casos > 0; casos--) {
			
			int f = src.nextInt()/2;
			int c = src.nextInt();
			
			int valor;
			int [] xi = new int [c +1];
			int [] yi = new int [c +1];

			int [] xf = new int [c +1];
			int [] yf = new int [c +1];
			
			
			for (int i = 0; i < f; i++) {
				for (int j = 0; j < f; j++) {
					valor = src.nextInt();
					if (valor >= 1 && valor <= c) {
						xi[valor] = i;
						yi[valor] = j;
					}
				}
			}
			
			for (int i = 0; i < f; i++) {
				for (int j = 0; j < f; j++) {
					valor = src.nextInt();
					if (valor >= 1 && valor <= c) {
						xf[valor] = i;
						yf[valor] = j;
					}
				}
			}
			
			boolean correcte = true;
			f = 2;
			int dx = xi[1] - xf[1];
			int dy = yi[1] - yf[1];
			while (f <= c && correcte) {
				if (xi[f] - xf[f] != dx || yi[f] - yf[f] != dy) 
					correcte = false;
				f++;
			}
			
			if (correcte) {
				dx = Math.abs(dx);
				dy = Math.abs(dy);
				if (dx > dy)
					System.out.println(dx);
				else 
					System.out.println(dy);
			}
			
			else System.out.println("NO SINCRONITZADA");
				
		}
		
	}

}

