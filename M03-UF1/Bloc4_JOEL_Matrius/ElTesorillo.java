package matrius;

import java.util.Random;
import java.util.Scanner;

public class ElTesorillo {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Scanner src = new Scanner(System.in);
		
		int f = src.nextInt();
		int c = f;
		
		char[][] matriu = new char[f][c];
		
		Random rnd = new Random();
		int t = rnd.nextInt(f * c); // para que no se salga del cubo el tesoro
		
		int cont = 0;
		
		int contc = 0;
		
		for (int i = 0; i < f; i++) {
			for (int j = 0; j < c; j++) {
				contc++;
				if (contc == t) {
					matriu[i][j] = 'T';
				}
				
				else {
					matriu[i][j] = '0';
				}
	        }
        }
		
		int fc = 0;
		int cc = 0;
		
		for (int nintents = src.nextInt(); nintents > 0; nintents--) {
			
			fc = src.nextInt() - 1;
			cc = src.nextInt() - 1;
			
			cont++;
			
			System.out.println();
			
			System.out.println("Intent número: " + cont);
			
			if (matriu[fc][cc] == 'T') {
				System.out.println("Tressor: SI");
				nintents = 0;
			}
			
			else {
				matriu[fc][cc] = 'E';
				System.out.println("Tressor: NO");
			}
			
		}
		
		if (matriu[fc][cc] == 'E') {
			System.out.println();
			System.out.println("Mala sort, t'has quedat sense intents");
		}
		
		else if (matriu[fc][cc] == 'T') {
			System.out.println();
			System.out.println("HAS TROBAT EL TRESSOR!!!");
		}
		
		System.out.println();
		
		for (int i = 0; i < f; i++) {
			for (int j = 0; j < c; j++) {
				System.out.print(matriu[i][j] + " ");
	        }
				System.out.println();
        }
		
	}

}
