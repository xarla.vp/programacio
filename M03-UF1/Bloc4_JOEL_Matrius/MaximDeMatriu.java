package matrius;

import java.util.Scanner;

public class MaximDeMatriu {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Scanner src = new Scanner(System.in);
		
		for (int casos = src.nextInt(); casos > 0; casos--) {
			
			int f = src.nextInt();
			int c = src.nextInt();
			
			int[][] matriu = new int[f][c];
			
			for (int i = 0; i < f; i++) {
				for (int j = 0; j < c; j++) {
		            matriu[i][j] = src.nextInt();
		        }
	        }
			
			int max = matriu[0][0];
			int maxi = 0;
			int maxj = 0;
			
			for (int i = 0; i < f; i++) {
				for (int j = 0; j < c; j++) {
					if (matriu[i][j] > max){
						max = matriu[i][j];
						maxi = i;
						maxj = j;
					}
		        }
	        }
			
			System.out.println((maxi + 1) + " " + (maxj + 1));
		
		}
	}

}
