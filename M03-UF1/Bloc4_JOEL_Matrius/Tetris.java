package matrius;

import java.util.Scanner;

public class Tetris {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Scanner src = new Scanner(System.in);
		
		int f = src.nextInt();
		int c = src.nextInt();
		
		int[][] matriu = new int[f][c];
		
		for (int i = 0; i < f; i++) {
			for (int j = 0; j < c; j++) {
	            matriu[i][j] = src.nextInt();
	        }
        }
		
		int cont = 0;
		boolean cero = false;
		
		for (int i = 0; i < f; i++) {
			for (int j = 0; j < c; j++) {
				if (matriu[i][j] == 0) {
					cero = true;
				}
	        }
			if (cero == false) {
				cont++;
			}
			
			cero = false;
		
        }
		
		
		if (cont >= 4 ) {
			System.out.print("TETRIS");
		}
		
		else {
			System.out.print(cont);
		}
		
	}

}
