package matrius;

import java.util.Scanner;

public class QuadratMagic {

    public static void main(String[] args) {
        Scanner src = new Scanner(System.in);

        for (int casos = src.nextInt(); casos > 0; casos--) {
            int f = 3;
            int c = 3;

            int[][] matriu = new int[f][c];

            int sum = 0;

            boolean iguals = true;

            for (int i = 0; i < f; i++) {
                for (int j = 0; j < c; j++) {
                    matriu[i][j] = src.nextInt();
                }
            }

            for (int j = 0; j < c; j++) {
                sum += matriu[0][j];
            }

            for (int i = 0; i < f; i++) {
                int sumf = 0;
                for (int j = 0; j < c; j++) {
                    sumf += matriu[i][j];
                }

                if (sumf != sum) {
                    iguals = false;
                    break;
                }
            }

            for (int i = 0; i < f; i++) {
                for (int j = 0; j < c; j++) {
                    if (matriu[0][j] == matriu[1][j] || matriu[0][j] == matriu[2][j]) {
                        iguals = false;
                        break;
                    }
                }
            }

            for (int j = 0; j < c; j++) {
                int sumc = 0;

                for (int i = 0; i < f; i++) {
                    sumc += matriu[i][j];
                }

                if (sumc != sum) {
                    iguals = false;
                    break;
                }
            }

            int sumd1 = matriu[0][0] + matriu[1][1] + matriu[2][2];
            int sumd2 = matriu[0][2] + matriu[1][1] + matriu[2][0];

            if (sumd1 != sum || sumd2 != sum) {
                iguals = false;
            }

            if (iguals) {
                System.out.println("SI");
            } 
            
            else {
                System.out.println("NO");
            }
        }
    }
}
