package matrius;

import java.util.Scanner;

public class Killjoy {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Scanner src = new Scanner(System.in);
		
		
		for (int casos = src.nextInt(); casos > 0; casos--) {
			int f = src.nextInt();
			int c = f;
			
			int[][] matriu = new int[f][c];
			
			int cont = 0;
			int posj = 0;
			
			for (int i = 0; i < f; i++) {
				for (int j = 0; j < c; j++) {
		            matriu[i][j] = src.nextInt();
		            if (matriu[i][j] == 2) {
		            	posj = j;
		            }
		        }
	        }
			
			for (int i = 0; i < f; i++) {
				for (int j = 0; j < c; j++) {
					
					if (matriu[i][j] == 1 && j >= posj) {
						cont++;
					}
					
		        }
	        }
			
			System.out.println("Spotted enemies: " + cont);
			
		}
	}
}
