package matrius;

import java.util.Scanner;

public class TresEnTabla {

    public static void main(String[] args) {
        Scanner src = new Scanner(System.in);

        int contk = 0;
        int contm = 0;

        for (int casos = src.nextInt(); casos > 0; casos--) {
            int k = src.nextInt();
            int m = src.nextInt();

            int t = src.nextInt();
            int f = (int) Math.sqrt(t);
            int c = (int) Math.ceil((double) t / f);

            int[][] matriu = new int[f][c];

            for (int i = 0; i < f; i++) {
                for (int j = 0; j < c; j++) {
                    matriu[i][j] = src.nextInt();
                }
            }
            
            src.nextLine();
            
            String l = src.nextLine();

            if (l.equals("Si")) {
                int l1 = src.nextInt();
                int l2 = src.nextInt();
                
                src.nextLine();

                for (int i = 0; i < f; i++) {
                    int temp = matriu[i][l1];
                    matriu[i][l1] = matriu[i][l2];
                    matriu[i][l2] = temp;
                }
            }

            for (int i = 0; i < f; i++) {
                int consecutivos = 0;
                for (int j = 0; j < c; j++) {
                    if (matriu[i][j] == k) {
                        consecutivos++;
                        if (consecutivos == 3) {
                            contk++;
                            break;
                        }
                    } 
                    
                    else {
                        consecutivos = 0;
                    }
                    
                }
            }

            for (int i = 0; i < f; i++) {
                int consecutivos = 0;
                for (int j = 0; j < c; j++) {
                    if (matriu[j][i] == m) {
                        consecutivos++;
                        if (consecutivos == 3) {
                            contm++;
                            break;
                        }
                    } 
                    
                    else {
                        consecutivos = 0;
                    }
                    
                }
            }
        }

        if (contk == contm) {
            System.out.print(contk + " Empat");
        } 
        
        else if (contk > contm) {
            System.out.print(contk + " " + contm + " Kuan");
        } 
        
        else if (contm > contk) {
            System.out.print(contm + " " + contk + " Meloi");
        }

        System.out.println();
    }
}
