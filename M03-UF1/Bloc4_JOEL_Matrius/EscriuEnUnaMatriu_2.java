package matrius;

import java.util.Scanner;

public class EscriuEnUnaMatriu_2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Scanner src = new Scanner(System.in);
		
		int f = src.nextInt();
		int c = src.nextInt();
		
		int[][] matriu = new int[f][c];
		
		for (int i = 0; i < f; i++) {
			for (int j = 0; j < c; j++) {
	            matriu[i][j] = src.nextInt();
	        }
        }
		
		int cc = src.nextInt();
		int ff = src.nextInt();
		
		for (int i = 0; i < f; i++) {
			for (int j = 0; j < c; j++) {
				if (matriu[i][j] == cc) {
					matriu[i][j] = ff;
				}
				System.out.print(matriu[i][j] + " ");
	        }
				System.out.println();
        }
	}
}
