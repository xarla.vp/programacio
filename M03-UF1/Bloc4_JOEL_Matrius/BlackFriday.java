package matrius;

import java.util.Scanner;

public class BlackFriday {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Scanner src = new Scanner(System.in);
		
		int f = src.nextInt();
		int c = src.nextInt();
		
		int[][] matriu = new int[f][c];
		
		for (int i = 0; i < f; i++) {
			for (int j = 0; j < c; j++) {
	            matriu[i][j] = src.nextInt();
	        }
        }
		
		int num = src.nextInt();
		
		for (int i = 0; i < f; i++) {
			for (int j = 0; j < c; j++) {
				int mult = matriu[i][j] * num;
				System.out.print(mult + " ");
	        }
				System.out.println();
        }
		
	}

}
