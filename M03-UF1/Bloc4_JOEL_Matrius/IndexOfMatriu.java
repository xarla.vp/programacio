package matrius;

import java.util.Scanner;

public class IndexOfMatriu {

    public static void main(String[] args) {

        Scanner src = new Scanner(System.in);

        int f = src.nextInt();
        int c = src.nextInt();

        int[][] matriu = new int[f][c];

        for (int i = 0; i < f; i++) {
            for (int j = 0; j < c; j++) {
                matriu[i][j] = src.nextInt();
            }
        }

        int k = src.nextInt();
        boolean trobat = false;

        for (int i = 0; i < f; i++) {
            for (int j = 0; j < c; j++) {
                if (k == matriu[i][j]) {
                    System.out.println(i + " " + j);
                    trobat = true;
                }
            }
        }

        if (trobat == false) {
            System.out.println("-1 -1");
        }
    }
}
