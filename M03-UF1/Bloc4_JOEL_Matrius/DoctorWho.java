package matrius;

import java.util.Scanner;

public class DoctorWho {

    public static void main(String[] args) {

        Scanner src = new Scanner(System.in);

        int[][] matriu = {
                {0, 0, 2, 0, 0},
                {0, 2, 2, 2, 0},
                {2, 2, 1, 2, 2},
                {0, 2, 2, 2, 0},
                {0, 0, 2, 0, 0}
        };

        int ft = src.nextInt();
        int ct = src.nextInt();

        int fc = src.nextInt();
        int cc = src.nextInt();

        int dist = Math.abs(ft - fc) + Math.abs(ct - cc);

        boolean dins = dist <= 2;

        System.out.println(dins);
    }
}
