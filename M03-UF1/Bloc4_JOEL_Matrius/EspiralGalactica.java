package matrius;

import java.util.Scanner;

public class EspiralGalactica {
	public static void main(String[] args) {
		
		Scanner src = new Scanner(System.in);
		int entrada = src.nextInt();

		while (entrada != 0) {
			int[][] mat = new int[entrada][entrada];
			for (int i = 0; i < entrada; ++i) {
				for (int j = 0; j < entrada; ++j) {
					mat[i][j] = src.nextInt();
				}
			}

			int i = entrada / 2;
			int j = entrada / 2;

			int[][] dir = { { -1, 0 }, { 0, 1 }, { 1, 0 }, { 0, -1 } };

			int actual = 0;
			int num = 1;

			int suma = 0;
			int resta = 2;

			while (i >= 0 && i < entrada && j >= 0 && j < entrada) {
				suma = suma + mat[i][j];
				resta--;
				if (resta != 0) {
				}
				else {
					num++;
					resta = num;
					actual = (actual + 1) % 4;
				}
				i = i + dir[actual][0];
				j = j + dir[actual][1];

			}

			System.out.println(suma);

			entrada = src.nextInt();

		}
	}
}