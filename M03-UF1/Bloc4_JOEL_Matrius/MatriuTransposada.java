package matrius;

import java.util.Scanner;

public class MatriuTransposada {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Scanner src = new Scanner(System.in);
		
		int f = src.nextInt();
		int c = src.nextInt();
		
		int[][] matriu1 = new int[f][c];
		
		for (int i = 0; i < f; i++) {
			for (int j = 0; j < c; j++) {
	            matriu1[i][j] = src.nextInt();
	        }
        }
		
		int ff = f;
		f = c;
		c = ff;
		
		for (int i = 0; i < f; i++) {
			for (int j = 0; j < c; j++) {
				System.out.print(matriu1[j][i] + " ");
	        }
				System.out.println();
        }
		
	}

}
