package matrius;

import java.util.Scanner;

public class SumaMatrius {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Scanner src = new Scanner(System.in);
		
		int f = src.nextInt();
		int c = f;
		
		int[][] matriu1 = new int[f][c];
		int[][] matriu2 = new int[f][c];
		
		for (int i = 0; i < f; i++) {
			for (int j = 0; j < c; j++) {
	            matriu1[i][j] = src.nextInt();
	        }
        }
		
		for (int i = 0; i < f; i++) {
			for (int j = 0; j < c; j++) {
	            matriu2[i][j] = src.nextInt();
	        }
        }
		
		for (int i = 0; i < f; i++) {
			for (int j = 0; j < c; j++) {
				matriu1[i][j] = matriu1[i][j] + matriu2[i][j];
				System.out.print(matriu1[i][j] + " ");
	        }
				System.out.println();
        }
		
	}

}
