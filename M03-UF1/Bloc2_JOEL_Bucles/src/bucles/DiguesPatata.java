package bucles;

import java.util.Scanner;

public class DiguesPatata {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Scanner src = new Scanner(System.in);
		
		String missatge;
		
		int num = src.nextInt();
		
		src.nextLine();
		
		while (num > 0) {
			missatge = src.nextLine();
			System.out.print(missatge);
			num--;
		}
		
	}

}