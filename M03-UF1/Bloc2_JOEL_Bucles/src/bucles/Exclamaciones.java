package bucles;

import java.util.Scanner;

public class Exclamaciones {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Scanner src = new Scanner(System.in);
		
		String paraula = "";
		
		int cont1 = 0;
		
		int cont2 = 0;
		
		while (true) {
			cont1 = 0;
			cont2 = 0;
			paraula = src.nextLine();
			
			if (paraula.equals("FIN")) {
				break;
			}
			
			if (paraula.contains("!")) {
				cont1++;
			}
			
			if (paraula.contains("¡")){
				cont2++;
			}
			
			if (cont1 == cont2) {
				System.out.print("SI");
			}
			
			else if (cont1 != cont2){
				System.out.print("NO");
			}
			
		}
		
		
		
	}

}