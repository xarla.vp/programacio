package bucles;

import java.util.Scanner;

public class ContarLA {
    public static void main(String[] args) {
        Scanner src = new Scanner(System.in);

        int casos = src.nextInt();
        src.nextLine();

        while (casos > 0) {
            String frase = src.nextLine().toLowerCase();
            int contla = 0;
            int pos = 1;

            while (pos < frase.length()) {
                if (frase.charAt(pos -1) == 'l' && frase.charAt(pos) == 'a') {
                    contla++;
                }
                pos++;
            }

            System.out.println(contla);
            casos--;
        }
    }
}
