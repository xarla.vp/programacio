package bucles;

import java.util.Scanner;

public class CalcularFactorial {

    public static void main(String[] args) {
        Scanner src = new Scanner(System.in);

        int cases = src.nextInt();

        for (int i = 0; i < cases; i++) {
            int num = src.nextInt(); 
            long factorial = 1;

            for (int j = 1; j <= num; j++) {
                factorial *= j;
            }

            System.out.println(factorial);
        }
    }
}