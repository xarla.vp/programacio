package bucles;

import java.util.Scanner;

public class UnBuenMomentoParaUnaBuenaBanana {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner src = new Scanner(System.in);
		
		int casos = src.nextInt();
		int dany;
		int vida;
		int vint;
		
		while (casos > 0) {
			
			vida = src.nextInt();
			dany = src.nextInt();
			
			vint = vida / 100 * 20;
			
			if (dany > vint && dany < vida) {
				System.out.print("Momento Banana \n ");
			}
			
			else {
				System.out.print("Nope");
			}
			
			casos--;
		}
		
	}

}
