package bucles;

import java.util.Scanner;

import java.util.ArrayList;

public class CalcularDivisors {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Scanner src = new Scanner(System.in);
		
		int casos = src.nextInt();	
				
		while (casos > 0) {
			ArrayList<Integer> nums = new ArrayList<Integer>();
			int num = src.nextInt();
			int num2 = 1;
			
			while (num2 <= num) {
				
				if (num % num2 == 0) {
				nums.add(num2);
				}
				num2++;
				
			}
			 nums.forEach(n -> System.out.print(n + " "));

	            if (!nums.isEmpty()) {
	                System.out.print("\n");
	            }
	            
			casos--;
		}
		
	}

}
