package bucles;

import java.util.Scanner;

public class SumaPositiusNegatius {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Scanner src = new Scanner(System.in);
		
		int pos = 0;
        
        int neg = 0;
        
        int n;
        
        while (true) {
	        	
	            n = src.nextInt();
			        
			        if (n < 0) {
	            		neg++;
		            }
	            	
			        else if (n > 0) {
	            		pos++;
		            }
			        
			        else if (n == 0) {
				        break;
				    }
		            
	    }
        
        if (pos > neg) {
        	System.out.println("POSITIUS");
        }
        
        else if (neg > pos) {
        	System.out.println("NEGATIUS");
        }
        
        else if (neg == pos) {
        	System.out.println("IGUALS");
        }
        
	}

}