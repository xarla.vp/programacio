package bucles;

import java.util.Scanner;

public class NumerosPositius {

    public static void main(String[] args) {

        Scanner src = new Scanner(System.in);

        int n;
        int pos = 0;

        n = src.nextInt();

        while (n > 0) {
            int numero = src.nextInt();
            
            if (numero > 0) {
                pos++;
            }
            
            n--;
        }

        System.out.print(pos);
    }
}