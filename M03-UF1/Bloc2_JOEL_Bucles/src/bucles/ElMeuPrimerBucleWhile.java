package bucles;

import java.util.Scanner;

public class ElMeuPrimerBucleWhile {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Scanner src = new Scanner(System.in);
		
		int num;
		
		while (true) {
			num = src.nextInt();
			if (num == 0) {
				break;
			}
			System.out.print(num + 1);
		}
		
	}

}