package bucles;

import java.util.Scanner;

public class ContadorsDeNotes {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Scanner src = new Scanner(System.in);
		
		int notes = 0;
		float mitj = 0;
		int e = 0;
		int n = 0;
		int b = 0;
		int s = 0;
		int i = 0;
		int md = 0;
		
		while (true) {
			int nota = src.nextInt();
			
			if (nota == -1) {
				break;
			}
			
			if (nota >= 0 && nota <= 10) {
				notes++;
				mitj = mitj + nota;
			}
			
			if (nota >= 9 && nota <= 10) {
				e++;
			}
			
			if (nota >= 7 && nota <= 8) {
				n++;
			}
			
			if (nota == 6) {
				b++;
			}
			
			if (nota == 5) {
				s++;
			}
			
			if (nota > 3 && nota < 5) {
				i++;
			}
			
			if (nota >= 0 && nota <= 3) {
				md++;
			}
			
		}
		
		System.out.println("NOTES: " + notes + " MITJANA: " + (mitj/notes) + " E: " + e + " N: " + n + " B: " + b + " S: " + s + " I: " + i + " MD: " + md);
    
	}

}