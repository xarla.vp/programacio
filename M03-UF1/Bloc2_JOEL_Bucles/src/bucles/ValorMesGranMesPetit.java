package bucles;

import java.util.Scanner;

public class ValorMesGranMesPetit {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Scanner src = new Scanner(System.in);
		
		int num = 1;
        
        int gran = 0;
        
        int petit = 0;
        
        while (true) {
	        	
	            num = src.nextInt();
			    
	            if (num == 0) {
	            	break;
	            }
	            
	            if (gran == 0 || num >= gran) {
            		gran = num;
            	}
				    
	            if (petit == 0 || num <= petit) {
            		petit = num;
            	}
	            
	    }
		
        System.out.print(gran + " " + petit);
        
	}

}