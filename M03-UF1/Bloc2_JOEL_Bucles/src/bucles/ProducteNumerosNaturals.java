package bucles;

import java.util.Scanner;

public class ProducteNumerosNaturals {

    public static void main(String[] args) {
        Scanner src = new Scanner(System.in);

        int k = src.nextInt();
        
        if (k > 0) {
	        while (k > 0) {
	            int n = src.nextInt();
	
	            if (n <= 0) {
	                System.out.println("ELS NOMBRES NATURALS COMENCEN EN 1");
	            } else {
	                int suma = 0;
	                int prod = 1;
	
	                while (n > 0) {
	                	suma = suma + n;
	                	prod = prod * n;
	                	n--;
	                }
	
	                System.out.println("SUMA: " + suma + " PRODUCTE: " + prod);
	            }
	
	            k--;
	        }
        }
        
    }
    
}