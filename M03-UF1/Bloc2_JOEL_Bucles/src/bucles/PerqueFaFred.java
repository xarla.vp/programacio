package bucles;

import java.util.Scanner;

public class PerqueFaFred {

    public static void main(String[] args) {
    	
        Scanner src = new Scanner(System.in);
        
        float temp = 0;
        float fred = 0;
        float tempfred = 0;

        while (src.hasNext()) {
            temp = src.nextFloat();
            if (temp < 0) {
                fred++;
                tempfred = tempfred + temp;
            }
        }

        if (fred > 0) {
            System.out.println((int) fred);
            System.out.println(tempfred / fred);
        }
    }
}
