package bucles;

import java.util.Scanner;

public class UnaDeCada {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Scanner src = new Scanner(System.in);
		
		int casos;
		
		String frase;
		String solucio;
		int i;
		boolean majus = false;
		
		casos = src.nextInt();
		src.nextLine();
		
		
		while (casos > 0) {
			frase = src.nextLine();
			majus = false;
			solucio = "";
			i = 0;
			
			while (i < frase.length()) {
				if ((frase.charAt(i) >= 'A' && frase.charAt(i) <= 'Z') || (frase.charAt(i) >= 'a' && frase.charAt(i) <= 'z')) {
					
					if (majus == true) {
						solucio = solucio + Character.toUpperCase(frase.charAt(i));
						majus = false;
					} else {
						solucio = solucio + Character.toLowerCase(frase.charAt(i));
						majus = true;
					}
				}
				
				else {
					solucio = solucio + frase.charAt(i);
				}
				
				i++;
				
			}
			
			System.out.println(solucio);
		}
		
	}

}
