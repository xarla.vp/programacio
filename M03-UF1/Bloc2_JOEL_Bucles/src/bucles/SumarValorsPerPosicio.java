package bucles;

import java.util.Scanner;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class SumarValorsPerPosicio {

    public static void main(String[] args) {
    	
        Scanner src = new Scanner(System.in);
        int casos = src.nextInt();
        int q;
        int n;
        int pos;
        int suma;
        
        while (casos > 0) {
        	q = src.nextInt();
        	pos = 0;
        	suma = 0;
        	while ( q > 0) {
        		n = src.nextInt();
        		pos++;
        		if (pos % 3 == 0) {
        			suma = suma + n;
        		}
        		q--;
        		  
        	}  
        	
        	casos --;
        	System.out.print(suma); 
        	
        }

    }
}
