package bucles;

import java.util.Scanner;

public class ComptarVocals {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Scanner src = new Scanner(System.in);
		
		int casos = src.nextInt();
		
		src.nextLine();
		
		while (casos > 0) {
			String num = src.nextLine(); 
			int pos = 0;
			int A = 0;
			int E = 0;
			int I = 0;
			int O = 0;
			int U = 0;
			
			while (pos < num.length()) {
				if (num.charAt(pos) == 'A' || num.charAt(pos) == 'a' ) {
					A++;
				}
				else if (num.charAt(pos) == 'E' || num.charAt(pos) == 'e' ) {
					E++;
				}
				else if (num.charAt(pos) == 'I' || num.charAt(pos) == 'i' ) {
					I++;
				}
				else if (num.charAt(pos) == 'O' || num.charAt(pos) == 'o' ) {
					O++;
				}
				else if (num.charAt(pos) == 'U' || num.charAt(pos) == 'u' ) {
					U++;
				}

				pos++;
				
			}
			
			System.out.print("A: " + A + " E: " + E + " I: " + I + " O: " + O + " U: " + U);
			casos--;
		}
		
	}

}
