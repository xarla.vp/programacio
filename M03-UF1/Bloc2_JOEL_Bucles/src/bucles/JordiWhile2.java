package bucles;

import java.util.Scanner;

public class JordiWhile2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Scanner src = new Scanner(System.in);
		
		int menosuno = 0;
		
		int espectadors;
		
		int total = 0;
		
		while (menosuno < 3) {
			espectadors = src.nextInt();
			
			if (espectadors == -1) {
				menosuno++;
			}
			
			else {
				total = total + espectadors;
			}
			
		}
		
		System.out.print(total);
		
	}

}