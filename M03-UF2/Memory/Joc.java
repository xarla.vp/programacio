package Memory;

import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

/** Classe a on es desenvolupa el funcionament principal del joc 
 * - El métode de jugar rebrà els dos jugadors actuals **/

public class Joc {

	static Scanner src = new Scanner(System.in);
	static Random rnd = new Random();

	public static int jugarPartida(String jugador1, String jugador2) {
		
		// VARIABLES NECESSÀRIES
				int guanyador = 0;									// Defineix qui ha guanyat, 0 = no ha guanyat ningú
					
				boolean acabat = false;
				
				char[][] tauler = new char[4][4];					// Crea un tauler buit a l'inici del programa
				char[][] taulerResolt = new char[4][4];				// Crea un tauler aleatori de parelles de lletres
				
				int jugador = 0;
				
				int punts1 = 0, punts2 = 0;							// Emmagatzema quants punts té cadascun per determinar el resultat
				int fil1 = 0, col1 = 0, fil2 = 0, col2 = 0;			// Primera i segona casella de cada jugador
				
				ArrayList<Character> lletres = new ArrayList<>();	// Llista de lletres a utilitzar aleatoriament (sense repetir)
				
		        omplirLlista(lletres);
				
				crearTaulers(tauler, taulerResolt);
				
				omplirTaulerResolt(lletres, taulerResolt);
				
				// COMENÇA LA PARTIDA
				
				do {
					
					comprovarEmpat(tauler, guanyador, acabat);
					
					mostrarTauler(tauler, taulerResolt, jugador1, jugador2, punts1, punts2);
					
					jugador = canviarTorn(jugador1, jugador2, jugador);
					
					if (!acabat) {
				        fil1 = demanarFila();
				        col1 = demanarColumna();
				        destaparCasella(tauler, taulerResolt, fil1, col1);
				        acabat = comprovarTauler(tauler);
				    }           

					comprovarEmpat(tauler, guanyador, acabat);

				    mostrarTauler(tauler, taulerResolt, jugador1, jugador2, punts1, punts2);

				    fil2 = demanarFila();
				    col2 = demanarColumna();
				    
				    destaparCasella(tauler, taulerResolt, fil2, col2);
				    
				    mostrarTauler(tauler, taulerResolt, jugador1, jugador2, punts1, punts2);
				    
				    comprovarCasella(tauler, taulerResolt, fil1, col1, fil2, col2, punts1, punts2, jugador, jugador1, jugador2);

					comprovarEmpat(tauler, guanyador, acabat);
				    
				    guanyador = comprovarGuanyador(tauler, acabat, punts1, punts2, jugador1, jugador2, guanyador);
				    				
					} while (acabat == false);
				
				return guanyador;
				
			}

			private static int canviarTorn(String jugador1, String jugador2, int jugador) {
				
				if (jugador == 1) {
					jugador = 2;
					System.out.println(jugador2 + ", el teu torn: ");
				}
				
				else {
					jugador = 1;
					System.out.println(jugador1 + ", el teu torn: ");
				}
				
				System.out.println();

				return jugador;
			}

			private static void omplirTaulerResolt(ArrayList<Character> lletres, char[][] taulerResolt) {
				
				for (int i = 0; i < 4; i++) {
				    for (int j = 0; j < 2; j++) { 
				    	
				        int fila;
				        int columna;

				        do {
				            fila = rnd.nextInt(4);
				            columna = rnd.nextInt(4);
				        } 
				        
				        while (taulerResolt[fila][columna] != '▉');

				        int index = rnd.nextInt(lletres.size());
				        char lletra = lletres.get(index);
				        lletres.remove(index);

				        taulerResolt[fila][columna] = lletra;
				        
				        do {
				            fila = rnd.nextInt(4);
				            columna = rnd.nextInt(4);
				        } 
				        
				        while (taulerResolt[fila][columna] != '▉');

				        taulerResolt[fila][columna] = lletra;
				    }
				}
		
			}

			private static void omplirLlista(ArrayList<Character> lletres) {
				
				for (char lletra = 'A'; lletra <= 'Z'; lletra++) {
		            lletres.add(lletra);
		        }
				
			}

			private static void crearTaulers(char[][] tauler, char[][] taulerResolt) {
				
				for (int i = 0; i < 4; i++) {
					for (int j = 0; j < 4; j++) {
			            tauler[i][j] = '▉';
			        }
		        }
				
				for (int i = 0; i < 4; i++) {
					for (int j = 0; j < 4; j++) {
			            taulerResolt[i][j] = '▉';
			        }
		        }
		
			}

			private static void comprovarEmpat(char[][] tauler, int guanyador, boolean acabat) {
				if (!casellesDisponibles(tauler) && guanyador == 0) {
		            System.out.println("Empat! No queden més caselles disponibles.");
		            acabat = true;
		        }
		
			}

			private static void comprovarCasella(char[][] tauler, char[][] taulerResolt, int fil1, int col1, int fil2, int col2, int punts1, int punts2, int jugador, String jugador1, String jugador2) {
								
			    if (tauler[fil1][col1] == tauler[fil2][col2]) {
			    	System.out.println();
		            System.out.print("Son la mateixa lletra !");
		            System.out.println();
		            
			        if (jugador == 1) {
			            punts1++;
			        } 
			        
			        else if (jugador == 2) {
			            punts2++;
			        } 
			        
			        System.out.print("Punts de " + jugador1 + ": " + punts1);
					System.out.println();
					System.out.print("Punts de " + jugador2 + ": " + punts2);
			    
			    } 
			    
			    else {
			        System.out.println();
			        System.out.print("No son la mateixa lletra :( ");
			        tauler[fil1][col1] = '▉';
			        tauler[fil2][col2] = '▉';
			        System.out.println();
			        System.out.println();
			    }
			    
			}

			private static int demanarColumna() {
				int col = 0;
				
				try {
					System.out.print("Digues una columna: ");
					col = src.nextInt();
					src.nextLine();
					return col;
				}
				
				catch (Exception e){
					System.out.println("Error, opció invàlida ");
					src.nextLine();
				}
				return col;
			}

			private static int demanarFila() {
				
				int fil = 0;
				
				try {
					System.out.print("Digues una fila: ");
					fil = src.nextInt();
					src.nextLine();
					return fil;
				}
				
				catch (Exception e){
					System.out.println("Error, opció invàlida ");
					src.nextLine();
				}
				return fil;
			}

			private static boolean casellesDisponibles(char[][] tauler) {
				
				for (int i = 0; i < 4; i++) {
			        for (int j = 0; j < 4; j++) {
			            if (tauler[i][j] == '▉') {
			                return true;
			            }
			        }
			    }
				
			    return false;
			}

			private static char[][] destaparCasella(char[][] tauler, char[][] taulerResolt, int fil1, int col1) {
		        
				tauler[fil1][col1] = taulerResolt[fil1][col1];

		        return tauler;
		    }

			private static void mostrarTauler(char[][] tauler, char[][] taulerResolt, String jugador1, String jugador2, int punts2, int punts1) {
				System.out.println();
				System.out.println("--------------- ESTAT DE LA PARTIDA ---------------");System.out.println();
				System.out.println();
				
				System.out.println("TAULER: ");
				System.out.println();
				
				for (int i = 0; i < 4; i++) {
					for (int j = 0; j < 4; j++) {
						System.out.print("  " + tauler[i][j] + "  ");
			        }
						System.out.println();
						System.out.println();
		        }
				
				System.out.println();
				System.out.println();
				System.out.println("--------------- ------------------- ---------------");
				System.out.println();
			}

			private static boolean comprovarTauler(char[][] tauler) {
			    boolean acabat = true;
			    
			    for (int i = 0; i < 4; i++) {
					for (int j = 0; j < 4; j++) {
						if (tauler[i][j] == '▉') {
				            acabat = false;
				        }
					}
		        }

			    return acabat;
			}
			
			private static int comprovarGuanyador(char[][] tauler, boolean acabat, int punts1, int punts2, String jugador1, String jugador2, int guanyador) {
				
				if (acabat == true) {
					
					if (punts1 > punts2) {
						System.out.println("FELICITATS " + jugador2 + ", has gunayat el joc !");
						System.out.println();
					}
					
					else if (punts2 > punts1) {
						System.out.println("FELICITATS " + jugador1 + ", has gunayat el joc !");
						System.out.println();
					}
					
					else {
						System.out.println("No ha guanyat ningú!!");
					}
				}
				
				return guanyador;
				
			}
	}
