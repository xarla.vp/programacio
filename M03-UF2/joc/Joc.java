package joc;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

/** Classe a on es desenvolupa el funcionament principal del joc 
 * - El métode de jugar rebrà el jugador actual**/

public class Joc {

	static Scanner src = new Scanner(System.in);

	public static boolean jugarPartida(String jugador) {
		
		// VARIABLES NECESSÀRIES
		boolean guanya = false; 		// Desa el resultat de la partida
		ArrayList<String> paraules; 	// Desa una llista de paraules a esbrinar
		String paraula = "";			// Desa la paraula escollida
		char[] palUsuari; 				// Desa la paraula construida pel jugador
		int intents = 0;				// Desa el número d'intents que ha gastat el jugador
		ArrayList<Character> lletres;	// Desa les lletres que encara no s'han fet servir
		char lletra;					// Desa la lletra escollida pel Jugador
		final int MAX = 12;				// Nombre màxim d'intents
		
		// INICIALITZEM LES VARIABLES PER A LA PARTIDA
		paraules = carregaParaules();
		lletres = carregaLletres();
		paraula = seleccionarParaula(paraules);
		palUsuari = crearPalUsuari(paraula);
		
		// COMENÇA LA PARTIDA
		
		do {
			mostrarEstatPartida(intents, lletres, palUsuari);
			lletra = demanarLletra(lletres);
			intents++;
			actualitzarPalUsuari(lletra, paraula, palUsuari, lletres);
			guanya = comprovarFinal(palUsuari, paraula);
		}
		
		while (intents < MAX && guanya == false);
		
		if (guanya == true) {
			System.out.println("FELICITATS, has gunayat el joc !");
			System.out.println();
		}
		
		else {
			System.out.println("Has perdio, ere malo.");
			System.out.println();
		}
		
		return guanya;
	}

	private static ArrayList<String> carregaParaules() {
		ArrayList<String> paraules = new ArrayList<>(Arrays.asList("DANI GUARRA", "MANZANA", "NARANJA", "DAVI", "BANANA", "KIWI", "UVA", "FRESA", "SANDÍA", "PERA", "MELÓN", "PIÑA"));
		return paraules;
	}

	private static ArrayList<Character> carregaLletres() {
		ArrayList<Character> lletres = new ArrayList<>(Arrays.asList('A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'Ñ', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'));
		return lletres;
	}

	private static String seleccionarParaula(ArrayList<String> paraules) {
		Random rnd = new Random();
		int posPar = rnd.nextInt(paraules.size());
		
		String paraula = paraules.get(posPar);
		return paraula;
	}

	private static char[] crearPalUsuari(String paraula) {
		char[] palUsuari = new char[paraula.length()];
		
		for (int i = 0; i < paraula.length(); i++) {
			palUsuari[i] = '_';
		}
		
		return palUsuari;
	}

	private static void mostrarEstatPartida(int intents, ArrayList<Character> lletres, char[] palUsuari) {
		System.out.println();
		System.out.println("--------------- ESTAT DE LA PARTIDA ---------------");
		System.out.println();
		System.out.println("INTENTS: " + intents);
		System.out.println();
		System.out.println("LLETRES DISPONIBLES: ");
		System.out.print(" - ");

		for (int i = 0; i < lletres.size(); i++) {
			System.out.print(lletres.get(i) + " - ");
		}
		
		System.out.println();
		System.out.println();
		
		System.out.println("PARAULA: ");
		
		for (int i = 0; i < palUsuari.length; i++) {
			System.out.print(palUsuari[i] + " ");
		}
		System.out.println();
		System.out.println();
		System.out.println("--------------- ------------------- ---------------");
		System.out.println();
	}

	private static char demanarLletra(ArrayList<Character> lletres) {
		char lletra;

	    do {
	        System.out.println("Lletra: ");
	        lletra = src.nextLine().toUpperCase().charAt(0);

	        if (!lletres.contains(lletra)) {
	            System.out.println("Lletra incorrecta");
	        }
	    } while (!lletres.contains(lletra));

	    lletres.remove((Character) lletra);
	    
		System.out.println();
	    
	    return lletra;
	}

	private static void actualitzarPalUsuari(char lletra, String paraula, char[] palUsuari, ArrayList<Character> lletres) {
		
		for (int i = 0; i < paraula.length(); i++) {
			if (paraula.charAt(i) == lletra) {
				palUsuari[i] = lletra;
			}
		}
		
	}

	private static boolean comprovarFinal(char[] palUsuari, String paraula) {
		
		boolean iguals = true;
		
		int i = 0;
		
		while ( i < paraula.length() && iguals == true) {
			if (paraula.charAt(i) != palUsuari[i]) {
				iguals = false;
			}
			else {
				i++;
			}
		}
		
		return iguals;
	}

}
