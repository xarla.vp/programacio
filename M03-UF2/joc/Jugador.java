package joc;

import java.util.Hashtable;
import java.util.Scanner;

/** Classe que gestiona la informació sobre els jugadors i fa:
 * - Alta de Jugadors
 * - Baixa de Jugadors
 * - Actualitzar partides guanyades
 * - Consultar partides guanyades**/

public class Jugador {
	
	static Hashtable<String, Integer> jugadors = new Hashtable<String, Integer> ();
	static Scanner src = new Scanner(System.in);
	
	public static String definirJugador() {
		String nom;
		System.out.println("Indica el teu nom: ");
		nom = src.nextLine();
		
		if (!jugadors.containsKey(nom)) {
			jugadors.put(nom, 0);
		}
		
		return nom;
	}

	public static void actualitzarResultat(String jugador, boolean resultat) {
		if (jugadors.containsKey(jugador) && resultat == true) {
			jugadors.put(jugador, jugadors.get(jugador) + 1);
		}
	}

	public static void veureResultat(String jugador) {
		if (jugadors.containsKey(jugador)) {
			System.out.println("Partides guanyades per " + jugador + " : " + jugadors.get(jugador));
		}
	}

	public static void veureRanking() {
		for (String key : jugadors.keySet()) {
			System.out.println(key + " ==> " + jugadors.get(key));
		}
	}

}
