package Mastermind;

import java.util.Scanner;

/**
 * Classe que implementa el funcionament principal del joc Mastermind.
 * El mètode jugarPartida rep el jugador actual i li sol·licita una sèrie d'instruccions per manipular una de les caselles disponibles.
 * Al final, verifica si queden caselles o si ha explotat i mostra si el jugador ha guanyat.
 */

public class Joc {

    /** Scanner per a l'entrada de dades des de la consola. */
    static Scanner src = new Scanner(System.in);

    /**
     * Inicia i controla el desenvolupament d'una partida del joc Mastermind.
     * @param combinacio La combinació secreta generada pel programa principal.
     * @return 
     */
    public static boolean jugarPartida(int[] combinacio) {

        boolean guanyador = false; // Defineix si el jugador ha guanyat o no

        boolean acabat = false;
        
        int[] vecJugador = new int[4];

        char[] encertades = {' ', ' ', ' ', ' '};

        do {
            int op = Programa.llegirNum("Vols seguir jugant (1) o sortir (0)? ", "Error, introdueix un nombre vàlid entre 1 i 0");

            // COMENÇA LA PARTIDA
            switch (op) {
                case 1:
                	                	
                    mostrarVector(combinacio, vecJugador, encertades);
                    
                    
                    vecJugador = demanarNumeros();
                    

                    encertades = casellaCorrecta(vecJugador, combinacio);

                    acabat = comprovarEncertades(encertades);

                    if (acabat) {
                        System.out.println();
                        System.out.println("¡FELICITATS, has guanyat!");
                    }

                    break;

                case 0:
                    System.out.println();
                    System.out.println("Fi del Joc");
                    acabat = true;
                    break;

                default:
                    System.out.println("Error, introdueix un valor correcte");
                    System.out.println();

            }
        } while (!acabat);
        
		return guanyador;

    }

    /**
     * Comprova si totes les caselles han estat endevinades.
     * @param encertades Un array de caràcters que indica l'estat de les caselles.
     * @return true si totes les caselles han estat endevinades, false altrament.
     */
    private static boolean comprovarEncertades(char[] encertades) {

        boolean acabat = true;

        for (int i = 0; i < encertades.length; i++) {
            if (encertades[i] != '*') {
                acabat = false;
            }
        }

        return acabat;
    }

    /**
     * Comprova i assigna l'estat de les caselles en funció dels nombres introduïts pel jugador i la combinació secreta.
     * @param vecJugador Els nombres introduïts pel jugador.
     * @param combinacio La combinació secreta.
     * @return Un array de caràcters que indica l'estat de les caselles.
     */
    private static char[] casellaCorrecta(int[] vecJugador, int[] combinacio) {

        char[] encertades = {' ', ' ', ' ', ' '}; // Reiniciem els encerts

        // Comprovem si hi ha encerts en la resta de nombres
        for (int i = 0; i < vecJugador.length; i++) {
            for (int j = 0; j < combinacio.length; j++) {
                if (vecJugador[i] == combinacio[j]) {
                    encertades[i] = '?';
                }
            }
        }

        // Comprovem si hi ha encerts complets
        for (int i = 0; i < vecJugador.length; i++) {
            if (vecJugador[i] == combinacio[i]) {
                encertades[i] = '*';
            }
        }

        return encertades;
    }

////////----------
    
    /**
     * Sol·licita al jugador un nombre per a una casella específica.
     * @return El nombre introduït pel jugador.
     */
    private static int[] demanarNumeros() {
        String num = "";
        int[] vecJugador = new int[4];
        
        boolean correcte = false;
        
        while (!correcte) {
        	System.out.println();
        	System.out.println("Ingresa quatre nombres del 0 al 9 (sense repeticions): ");
        	num = src.nextLine();
        	
        	correcte = comprovarNumeros(num);
        	
        	if (correcte) {
        		correcte = comprovarCombinacio(num);
        	}
        	
        }
        
        for (int i = 0; i < 4; i++) {
        	vecJugador[i] = num.charAt(i) - '0';
        }
        
        return vecJugador;
    }
    
    /**
     * Comprova si la combinació té nombres repetits.
     * @param num La combinació introduïda pel jugador.
     * @return true si la combinació no té nombres repetits, false altrament.
     */
    public static boolean comprovarCombinacio(String num) {
    	
    	boolean correcte = true;
    	
    	if ((num.charAt(0) == num.charAt(1)) ||
			(num.charAt(0) == num.charAt(2)) ||
			(num.charAt(0) == num.charAt(3)) ||
			(num.charAt(1) == num.charAt(2)) ||
			(num.charAt(1) == num.charAt(3)) ||
			(num.charAt(2) == num.charAt(3))) {
    		correcte = false;
    	}
    	
    	return correcte;
	}


    
    /**
     * Comprova si els nombres introduïts pel jugador són vàlids (sense caràcters no numèrics i amb 4 dígits).
     * @param num Els nombres introduïts pel jugador.
     * @return true si els nombres són vàlids, false altrament.
     */
    public static boolean comprovarNumeros(String num) {
        int[] vecJugador = new int[4];
        boolean correcte = true;
        
        if (num.length() != 4) {
            correcte = false;
        } 
        
        else {
            for (int i = 0; i < 4; i++) {
            	
                if (num.charAt(i) < '0' || num.charAt(i) > '9') {
                    correcte = false;
                    vecJugador = demanarNumeros();
                } 
               
            }
        }
        return correcte;
    }

    
////////----------

	/**
     * Mostra l'estat actual de la partida, incloent els nombres introduïts pel jugador i els seus respectius encerts.
     * @param combinacio La combinació secreta.
     * @param vecJugador Els nombres introduïts pel jugador.
     * @param encertades Un array de caràcters que indica l'estat de les caselles.
     */
    private static void mostrarVector(int[] combinacio, int[] vecJugador, char[] encertades) {
        System.out.println();
        System.out.println("---------------- ESTAT DE LA PARTIDA ---------------");
        System.out.println();
        System.out.println();

        System.out.println("ENCERTADES = '*'");
        System.out.println("AL TAULER  = '?'");

        System.out.println();
        System.out.println();

        for (int i = 0; i < vecJugador.length; i++) {
            System.out.print(vecJugador[i] + " ");
        }

        System.out.println();

        for (int i = 0; i < encertades.length; i++) {
            System.out.print(encertades[i] + " ");
        }

        System.out.println();
    }

}
