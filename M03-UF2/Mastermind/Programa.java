package Mastermind;

import java.util.Random;
import java.util.Scanner;

/**
 * Classe que representa el programa principal del joc Mastermind.
 * Aquest programa funciona com a distribuïdor de tasques a través d'un menú d'opcions.
 */

public class Programa {

    /** Scanner per a l'entrada de dades des de la consola. */
    static Scanner src = new Scanner(System.in);
    /** Generador de nombres aleatoris. */
    static Random rnd = new Random();

    /**
     * Mètode principal que inicia l'execució del programa.
     * Permet a l'usuari generar una combinació secreta i jugar partides fins que decideixi sortir.
     * @param args Els arguments de línia de comandes (no s'utilitzen en aquest programa).
     */
    public static void main(String[] args) {
        int op = 0; // Emmagatzema l'opció seleccionada per l'usuari
        int[] combinacio = new int[4]; // Crea un vector de 4 nombres

        boolean generada = false; // Indica si hi ha una nova generació o no

        do {
            op = mostrarMenu();

            switch (op) {

                case 1:
                    combinacio = generarCombinacio();
                    generada = true;
                    break;

                case 2:
                    if (generada) { // Si no hi ha una nova generació, no es pot jugar
                        Joc.jugarPartida(combinacio);
                    } else {
                        System.out.println();
                        System.out.println("Error, has de generar una combinació abans de començar.");
                    }
                    break;

                case 0:
                    System.out.println();
                    System.out.println("Fi del Joc");
                    break;

                default:
                    System.out.println();
                    System.out.println("Error, introdueix un valor correcte");
            }
        }

        while (op != 0);

    }

    /**
     * Genera una combinació secreta aleatòria de quatre nombres.
     * @return La combinació generada.
     */
    private static int[] generarCombinacio() {
        int[] comb = new int[4];

        comb[0] = rnd.nextInt(9);

        do {
            comb[1] = rnd.nextInt(9);
        } while (comb[1] == comb[0]);

        do {
            comb[2] = rnd.nextInt(9);
        } while (comb[2] == comb[0] || comb[2] == comb[1]);

        do {
            comb[3] = rnd.nextInt(9);
        } while (comb[3] == comb[0] || comb[3] == comb[1] || comb[3] == comb[2]);

        return comb;
    }

    /**
     * Mostra el menú d'opcions i sol·licita a l'usuari que seleccioni una opció.
     * @return L'opció seleccionada per l'usuari.
     */
    private static int mostrarMenu() {
        System.out.println();
        System.out.println("-------------------- Mastermind -------------------");
        System.out.println("1.- Generar combinació");
        System.out.println("2.- Jugar partida");
        System.out.println("0.- Sortir");
        System.out.println("------------------ -------------- -----------------");
        System.out.println();

        int op = llegirNum("Escull una opció: ", "Error, opció invàlida.");

        return op;
    }

    /**
     * Llegeix un nombre enter des de la consola mostrant un missatge i un missatge d'error personalitzat si és necessari.
     * @param mensaje1 El missatge a mostrar abans de sol·licitar l'entrada.
     * @param mensaje2 El missatge d'error a mostrar si l'entrada no és vàlida.
     * @return El nombre enter ingressat per l'usuari.
     */
    static int llegirNum(String mensaje1, String mensaje2) {
        int num = 0;
        boolean correcte = false;

        do {
            System.out.println();
            System.out.println(mensaje1);

            try {
                num = src.nextInt();
                correcte = true;
            }

            catch (Exception e){
                System.out.println(mensaje2);
                src.nextLine();
            }
        }

        while (!correcte);

        return num;
    }
}
