package Mastermind;

import static org.junit.Assert.*;
import org.junit.Test;

/**
 * Classe de proves unitàries per a les funcionalitats de validació de nombres i combinacions en el joc Mastermind.
 */

public class MastermindTest {

    /**
     * Prova la funció de validació de nombres.
     * Es realitzen proves amb nombres vàlids de 4 dígits, nombres invàlids amb menys o més de 4 dígits
     * i nombres invàlids amb caràcters diferents a dígits numèrics.
     */
    @Test
    public void testComprovarNumeros() {
        // Vàlids de 4 nombres
        assertTrue(Joc.comprovarNumeros("1234"));
        assertTrue(Joc.comprovarNumeros("5678"));
        assertTrue(Joc.comprovarNumeros("9725"));
        assertTrue(Joc.comprovarNumeros("1111")); // Tots iguals, però vàlids
        
        // Invàlids de menys o més de 4 números
        assertFalse(Joc.comprovarNumeros("123")); // 3 nombres
        assertFalse(Joc.comprovarNumeros("12345")); // 5 nombres
        
        // Invàlids amb caràcters diferents a un número
        assertFalse(Joc.comprovarNumeros("abc")); // Caràcters no numèrics
        assertFalse(Joc.comprovarNumeros("12a4")); // Caràcter 'a'
        assertFalse(Joc.comprovarNumeros("12 34")); // Espai entre nombres
        assertFalse(Joc.comprovarNumeros("1, 2, 3, 4")); // Coma i espai entre nombres
    }

    /**
     * Prova la funció de validació de combinacions.
     * Es realitzen proves amb combinacions vàlides sense repetició i combinacions invàlides amb repetició.
     */
    @Test
    public void testComprovarCombinacio() {
        // Vàlides sense repetició
        assertTrue(Joc.comprovarCombinacio("1234"));
        assertTrue(Joc.comprovarCombinacio("9725"));
        
        // Invàlides amb repetició
        assertFalse(Joc.comprovarCombinacio("1123")); // 1 repetit
        assertFalse(Joc.comprovarCombinacio("2234")); // 1 repetit
        assertFalse(Joc.comprovarCombinacio("3311")); // 2 repetits
        assertFalse(Joc.comprovarCombinacio("4466")); // 2 repetits
        assertFalse(Joc.comprovarCombinacio("2229")); // 3 repetits
        assertFalse(Joc.comprovarCombinacio("3353")); // 3 repetits
        assertFalse(Joc.comprovarCombinacio("4444")); // 4 repetits
        assertFalse(Joc.comprovarCombinacio("1111")); // 4 repetits
    }
}
