package HundirLaFlota;

import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

/** Classe a on es desenvolupa el funcionament principal del joc 
 * - El métode de jugar rebrà els dos jugadors actuals **/

public class Joc {

	static Scanner src = new Scanner(System.in);
	static Random rnd = new Random();

	public static int jugarPartida(String jugador1, String jugador2) {
		
		// VARIABLES NECESSÀRIES
				int guanyador = 0;									// Defineix qui ha guanyat, 0 = no ha guanyat ningú
					
				boolean acabat = false;
				
				char[][] taulerJ1 = new char[8][8];					// Crea un tauler buit a l'inici del programa
				char[][] taulerJ2 = new char[8][8];				// Crea un tauler aleatori de parelles de lletres
				
				int jugador = 1;
				
				crearTaulers(taulerJ1);
				crearTaulers(taulerJ2);
				
				colocarVaixells(taulerJ1, jugador1);
				colocarVaixells(taulerJ2, jugador2);
				
				// COMENÇA LA PARTIDA
				
				do {
					
						mostrarTauler(taulerJ1, taulerJ2, jugador1, jugador2);
						
						if (jugador == 1) {
							disparar(taulerJ2, jugador2);
						}
						
						else {
							disparar(taulerJ1, jugador1);
						}
						
						acabat = comprovarTauler(taulerJ1);
						
						jugador = canviarTorn(jugador1, jugador2, jugador);
					
					} while (acabat == false);
				
				guanyador = comprovarGuanyador(taulerJ1, taulerJ2, acabat, jugador2, jugador2, jugador);
				
				return guanyador;
				
			}

			private static void disparar(char[][] tauler, String jugador) {
			    int fil, col;
			    
			    boolean correcte = false;
			    System.out.println(jugador + ", realiza un disparo:");
		
			    do {
			        fil = demanarCoord("Fila: ");
			        col = demanarCoord("Columna: ");
		
			        if (tauler[fil][col] == 'X' || tauler[fil][col] == '*' || tauler[fil][col] == '-') {
			            System.out.println("Ya has disparat aquí. Escull una altra.");
			        } 
			        
			        else {
			            correcte = true;
			        }
		
			    } while (correcte == false);
		
			    if (tauler[fil][col] == 'X') {
			        System.out.println("Tocat! Li has donat a un vaixell.");
			        tauler[fil][col] = '*';
			    } 
			    
			    else {
			        System.out.println("Aigua. No li has donat a cap vaixell.");
			        tauler[fil][col] = '-';
			    }
			    
			}
			
			private static int demanarCoord(String string) {
				int coord = 0;
				boolean correcte = false;
				
		        do {
		            try {
		            	
		                System.out.print(string);
		                coord = src.nextInt();
		                src.nextLine();
		                if (coord >= 0 && coord < 8) {
		                    correcte = true;
		                } 
		                
		                else {
		                    System.out.println("Coordenada invàlida. Ha de ser un nombre entre 0 y 7.");
		                }
		                
		            } 
		            
		            catch (Exception e) {
		                System.out.println("Error, opció invàlida.");
		                src.nextLine();
		            }
		            
		        } 
		        
		        while (correcte = false);

		        return coord;
			}

			private static void colocarVaixells(char[][] tauler, String jugador) {
			    System.out.println(jugador + ", col·loca els teus vaixells:");
			    
			    colocacioVaixell(tauler, 4, jugador);
			    
			    colocacioVaixell(tauler, 3, jugador);
			    colocacioVaixell(tauler, 3, jugador);
			    
			    colocacioVaixell(tauler, 2, jugador);
			    colocacioVaixell(tauler, 2, jugador);
			    colocacioVaixell(tauler, 2, jugador);
			    
			    colocacioVaixell(tauler, 1, jugador);
			    colocacioVaixell(tauler, 1, jugador);
			    colocacioVaixell(tauler, 1, jugador);
			    colocacioVaixell(tauler, 1, jugador);
			}

			private static void colocacioVaixell(char[][] tauler, int mida, String jugador) {
			    boolean colocat = false;

			    while (!colocat) {
			        int filaInicial = demanarCoord("Fila per al vaixell (" + mida + " espais): ");
			        int colInicial = demanarCoord("Columna per al vaixell (" + mida + " espais): ");
			        char direccio = demanarDireccio();

			        if (colocacioValida(filaInicial, colInicial, direccio, mida, tauler)) {
			        	colocarVaixell(tauler, filaInicial, colInicial, direccio, mida);
			            System.out.println("Vaixell col·locat en fila " + filaInicial + ", columna " + colInicial);
			            colocat = true;
			        } 
			        
			        else {
			            System.out.println("Posició invàlida. Verifica que el vaixell encaixa correctament.");
			        }
			        
			    }
			}

			private static boolean colocacioValida(int fil, int col, char dir, int mida, char[][] tauler) {
			    if (dir == 'h') {
			        if (col + mida > 8) {
			            return false;
			        }
			        
			        for (int i = 0; i < mida; i++) {
			            if (tauler[fil][col + i] == 'X') {
			                return false; 
			            }
			        }
			    } 
			    
			    else if (dir == 'v') {
			        if (fil + mida > 8) {
			            return false;
			        }
			        
			        for (int i = 0; i < mida; i++) {
			            if (tauler[fil + i][col] == 'X') {
			                return false; 
			            }
			        }
			    } 
			    
			    else {
			        return false;
			    }
			    
			    return true;
			}

			private static void colocarVaixell(char[][] tauler, int fil, int col, char dir, int mida) {
			    if (dir == 'h') {
			        for (int i = 0; i < mida; i++) {
			            tauler[fil][col + i] = 'X';
			        }
			    } 
			    
			    else if (dir == 'v') {
			        for (int i = 0; i < mida; i++) {
			            tauler[fil + i][col] = 'X';
			        }
			    }
			}

			private static char demanarDireccio() {
			    char dir;
			    
			    do {
			        System.out.print("Introdueix la direcció del vaixell (H per horitzontal, V per vertical): ");
			        dir = src.next().toLowerCase().charAt(0);
			    } 
			    
			    while (dir != 'h' && dir != 'v');
			    
			    return dir;
			}

			private static int canviarTorn(String jugador1, String jugador2, int jugador) {
				
				if (jugador == 1) {
					jugador = 2;
					System.out.println(jugador2 + ", el teu torn: ");
				}
				
				else {
					jugador = 1;
					System.out.println(jugador1 + ", el teu torn: ");
				}
				
				System.out.println();

				return jugador;
			}

			private static void crearTaulers(char[][] tauler) {
				
				for (int i = 0; i < 8; i++) {
					for (int j = 0; j < 8; j++) {
			            tauler[i][j] = '_';
			        }
		        }
		
			}

			private static void mostrarTauler(char[][] taulerJ1, char[][] taulerJ2, String jugador1, String jugador2) {
				System.out.println();
				System.out.println("--------------- ESTAT DE LA PARTIDA ---------------");System.out.println();
				System.out.println();
				
				System.out.println("TAULER: ");
				System.out.println();
				
				for (int i = 0; i < 8; i++) {
					for (int j = 0; j < 8; j++) {
						System.out.print(taulerJ1[i][j] + "  ");
			        }
						System.out.println();
		        }
				
				System.out.println();
				System.out.println();
				
				for (int i = 0; i < 8; i++) {
					for (int j = 0; j < 8; j++) {
						System.out.print(taulerJ2[i][j] + "  ");
			        }
						System.out.println();
		        }
				
				System.out.println();
				System.out.println();
				System.out.println("--------------- ------------------- ---------------");
				System.out.println();
			}

			private static boolean comprovarTauler(char[][] tauler) {
			    boolean acabat = true;
			    
			    for (int i = 0; i < 8; i++) {
					for (int j = 0; j < 8; j++) {
						if (tauler[i][j] == 'X') {
				            acabat = false;
				        }
					}
		        }

			    return acabat;
			}
			
			private static int comprovarGuanyador(char[][] taulerJ1, char[][] taulerJ2, boolean acabat, String jugador1, String jugador2, int guanyador) {
				
				if (acabat == true) {
					
					if (!comprovarTauler(taulerJ1)) {
						System.out.println("FELICITATS " + jugador2 + ", has gunayat el joc !");
						System.out.println();
						guanyador = 2;
					}
					
					else {
						System.out.println("FELICITATS " + jugador1 + ", has gunayat el joc !");
						System.out.println();
						guanyador = 1;
					}
					
				}
				
				return guanyador;
				
			}
	}
