package TresEnTaula;

import java.util.Hashtable;
import java.util.Scanner;

/** Classe que gestiona la informació sobre els jugadors i fa:
 * - Alta de Jugadors
 * - Baixa de Jugadors
 * - Actualitzar partides guanyades
 * - Consultar partides guanyades**/

public class Jugador {
	
	static Hashtable<String, Integer> jugadors = new Hashtable<String, Integer> ();
	static Scanner src = new Scanner(System.in);
	
	public static String definirJugador() {
		String nom;
		System.out.println("Indica el teu nom: ");
		nom = src.nextLine();
		
		if (!jugadors.containsKey(nom)) {
			jugadors.put(nom, 0);
		}
		
		return nom;
	}

	public static void actualitzarResultat(String jugador1, String jugador2, int resultat) { // SI EL RESULTAT EL TRUE, GUANYA L'1. SINÓ GUANYA EL 2
		if (jugadors.containsKey(jugador1) && resultat == 1) {
			jugadors.put(jugador1, jugadors.get(jugador1) + 1);
		}
		else if (jugadors.containsKey(jugador2) && resultat == 2) {
			jugadors.put(jugador2, jugadors.get(jugador2) + 1);
		}
	}

	public static void veureResultat(String jugador1, String jugador2) {
		if (jugadors.containsKey(jugador1) && jugadors.containsKey(jugador2)) {
			System.out.println();
			System.out.println("Partides guanyades per " + jugador1 + " : " + jugadors.get(jugador1));
			System.out.println("Partides guanyades per " + jugador2 + " : " + jugadors.get(jugador2));
		}
	}

	public static void veureRanking() {
		for (String key : jugadors.keySet()) {
			System.out.println(key + " ==> " + jugadors.get(key));
		}
	}

}
