package TresEnTaula;

import java.util.Scanner;

/** Classe a on es desenvolupa el funcionament principal del joc 
 * - El métode de jugar rebrà els dos jugadors actuals **/

public class Joc {

	static Scanner src = new Scanner(System.in);

	public static int jugarPartida(String jugador1, String jugador2) {
		
		// VARIABLES NECESSÀRIES
		int guanyador = 0;									// Defineix qui ha guanyat, 0 = no ha guanyat ningú
			
		boolean acabat = false;
		
		char[][] tauler = new char[3][3];					// Crea un tauler buit a l'inici del programa
		
		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 3; j++) {
	            tauler[i][j] = '_';
	        }
        }
		
		// COMENÇA LA PARTIDA
		
		do {
			
			if (!casellesDisponibles(tauler) && guanyador == 0) {
	            System.out.println("Empat! No queden més caselles disponibles.");
	            acabat = true;
	        }
			
			mostrarTauler(tauler, jugador1, jugador2);
			
			if (!acabat) {
				System.out.print(jugador1 + ", ");
	            tauler = demanarCasella(tauler, 'X'); 		// Tauler per veure si algú ha guanyat
	            acabat = comprovarTauler(tauler);
	        }			
			
			if (!casellesDisponibles(tauler) && guanyador == 0) {
	            System.out.println("Empat! No queden més caselles disponibles.");
	            acabat = true;
	        }
			
			mostrarTauler(tauler, jugador1, jugador2);
			
			if (!acabat) {
				System.out.print(jugador2 + ", ");
	            tauler = demanarCasella(tauler, 'O');
	            acabat = comprovarTauler(tauler);
	        }
			
			guanyador = comprovarGuanyador(tauler, acabat);
		}
		
		while (acabat == false);
		
		if (guanyador == 1) {
			System.out.println("FELICITATS " + jugador1 + ", has gunayat el joc !");
			System.out.println();
		}
		
		else if (guanyador == 2) {
			System.out.println("FELICITATS " + jugador2 + ", has gunayat el joc !");
			System.out.println();
		}
		
		else {
			System.out.println("No ha guanyat ningú!!");
		}
		
		return guanyador;
	}

	private static boolean casellesDisponibles(char[][] tauler) {
		
		for (int i = 0; i < 3; i++) {
	        for (int j = 0; j < 3; j++) {
	            if (tauler[i][j] == '_') {
	                return true;
	            }
	        }
	    }
	    return false;
	}

	private static char[][] demanarCasella(char[][] tauler, char simbol) {
        int i = 0;
        int j = 0;

        do {
        	
            try {
                System.out.print("Digues una fila i una columna disponibles del tauler: ");
                i = src.nextInt();
                j = src.nextInt();

                if (i < 0 || i >= 3 || j < 0 || j >= 3 || tauler[i][j] != '_') {
                    System.out.println("Casella no vàlida. Torna a provar.");
                }
            } 
            
            catch (Exception e) {
                System.out.println("Entrada no vàlida. Introdueix valors numèrics.");
                
                src.nextLine();
            }

        } while (i < 0 || i >= 3 || j < 0 || j >= 3 || tauler[i][j] != '_');

        tauler[i][j] = simbol;

        return tauler;
    }

	private static void mostrarTauler(char[][] tauler, String jugador1, String jugador2) {
		System.out.println();
		System.out.println("--------------- ESTAT DE LA PARTIDA ---------------");
		System.out.print(jugador1 + " = X ");
		System.out.println();
		System.out.print(jugador2 + " = O ");
		
		System.out.println();
		System.out.println();
		
		System.out.println("TAULER: ");
		System.out.println();
		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 3; j++) {
				System.out.print(tauler[i][j] + " ");
	        }
				System.out.println();
        }
		
		System.out.println();
		System.out.println();
		System.out.println("--------------- ------------------- ---------------");
		System.out.println();
	}

	private static boolean comprovarTauler(char[][] tauler) {
	    boolean acabat = false;

	    // FILES
	    for (int i = 0; i < 3; i++) {
	        if (tauler[i][0] != '_' && tauler[i][1] == tauler[i][0] && tauler[i][2] == tauler[i][0]) {
	            acabat = true;
	        }
	    }

	    // COLUMNES
	    if (!acabat) {
	        for (int j = 0; j < 3; j++) {
	            if (tauler[0][j] != '_' && tauler[1][j] == tauler[0][j] && tauler[2][j] == tauler[0][j]) {
	                acabat = true;
	            }
	        }
	    }

	    // DIAGONALS
	    if (!acabat && tauler[0][0] != '_' && tauler[0][0] == tauler[1][1] && tauler[0][0] == tauler[2][2]) {
	        acabat = true;
	    }
	    
	    if (!acabat && tauler[0][2] != '_' && tauler[0][2] == tauler[1][1] && tauler[0][2] == tauler[2][0]) {
	        acabat = true;
	    }

	    return acabat;
	}
	
	private static int comprovarGuanyador(char[][] tauler, boolean acabat) {
		
		boolean trobat = false;
		int guanyador = 0;
		
		if (acabat == true) {
			
			// FILES
		    for (int i = 0; i < 3; i++) {
		        if (tauler[i][0] == 'X' && tauler[i][1] == 'X' && tauler[i][2] == 'X') {
		        	trobat = true;
		        	guanyador = 1;
		        }
		        
		        else if (tauler[i][0] == 'O' && tauler[i][1] == 'O' && tauler[i][2] == 'O') {
		        	trobat = true;
		        	guanyador = 2;
		        }
		    }

		    // COLUMNES
		    if (!trobat) {
		        for (int j = 0; j < 3; j++) {
		            if (tauler[0][j] == 'X' && tauler[1][j] == 'X' && tauler[2][j] == 'X') {
		            	trobat = true;
		            	guanyador = 1;
		            }
		            
		            else if (tauler[0][j] == 'O' && tauler[1][j] == 'O' && tauler[2][j] == 'O') {
		            	trobat = true;
		            	guanyador = 2;
		            }
		            
		        }
		    }

		    // DIAGONALS
		    if (!trobat && tauler[0][0] == 'X' && tauler[1][1] == 'X' && tauler[2][2] == 'X') {
		    	trobat = true;
		    	guanyador = 1;
		    }
		    
		    else if (!trobat && tauler[0][0] == 'O' && tauler[1][1] == 'O' && tauler[2][2] == 'O') {
		    	trobat = true;
		    	guanyador = 2;
		    }
		    
		    if (!trobat && tauler[0][2] == 'X' && tauler[1][1] == 'X' && tauler[2][0] == 'X') {
		    	trobat = true;
		    	guanyador = 1;
		    }
		    
		    else if (!trobat && tauler[0][2] == 'O' && tauler[1][1] == 'O' && tauler[2][0] == 'O') {
		    	trobat = true;
		    	guanyador = 2;
		    }
			
		}
		
		else {
			System.out.println("El joc encara no ha finalitzat");
		}
		
		return guanyador;
	}

}
