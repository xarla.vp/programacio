package TresEnTaula;

import java.util.Scanner;

/** Classe que llança l'aplicació, funciona com a distribuidor de tasques mitjançant un menú d'opcions **/

public class Programa {

	/** 1.- Mostrar Ajuda
		2.- Definir Jugadors
		3.- Jugar Partida
		4.- Veure Jugadors
		0.- Sortir			**/
	static Scanner src = new Scanner(System.in);
	
	public static void main(String[] args) {
		int op = 0; // Desa l'opció escollida per l'usuari
		String jugador1 = "";
		String jugador2 = "";
		int resultat = 0;
		
		boolean definit = false;
		
		do {
			op = mostrarMenu();
			
				switch (op) {
				case 1: 
					Ajuda.mostrarAjuda();
				break;
				
				case 2: 
					jugador1 = Jugador.definirJugador();
					jugador2 = Jugador.definirJugador();
					definit = true;
				break;
				
				case 3: 
					if (definit) {
	                    resultat = Joc.jugarPartida(jugador1, jugador2);
	                    Jugador.actualitzarResultat(jugador1, jugador2, resultat);
	                    definit = false;
	                } else {
	                    System.out.println("Error, has de definir els jugadors abans de començar.");
	                }
	            break;
				
				case 4: 
					Jugador.veureResultat(jugador1, jugador2);
				break;
				
				case 0: 
					System.out.println();
					System.out.println("Fi del Joc");
				break;
				
				default: 
					System.out.println("Error, introdueix un valor correcte");
					System.out.println();
			}
		}
		
		while (op != 0);
		
	}

	private static int mostrarMenu() {
		System.out.println();
		System.out.println("------------------ Tres En Taula ------------------");
		System.out.println("1.- Mostrar ajuda");
		System.out.println("2.- Definir jugadors");
		System.out.println("3.- Jugar partida");
		System.out.println("4.- Resultats jugador");
		System.out.println("0.- Sortir");
		System.out.println("------------------ -------------- -----------------");
		System.out.println();
		
		boolean correcte = false;
		
		int op = 0;
		
		do {
			System.out.println("Escull una opció: ");

			try {
				op = src.nextInt();
				correcte = true;
			}
			
			catch (Exception e){
				System.out.println("Error, opció invàlida ");
				src.nextLine();
			}
		}
		
		while (correcte == false);
		
		return op;
	}

}
