package EXAMEN;

import java.util.Random;
import java.util.Scanner;

/** Classe que llança l'aplicació, funciona com a distribuidor de tasques mitjançant un menú d'opcions **/

public class Programa {

	static Scanner src = new Scanner(System.in);
	static Random rnd = new Random();
	
	public static void main(String[] args) {
		int op = 0; // Desa l'opció escollida per l'usuari
		
		do {
			op = mostrarMenu();
			
				switch (op) {
				
				case 1: 
					Joc.jugarPartida(combinacio);
				break;
				
				case 2: 
					
	            break;
				
				case 0: 
					System.out.println();
					System.out.println("Fi del Joc");
				break;
				
				default: 
					System.out.println();
					System.out.println("Error, introdueix un valor correcte");
			}
		}
		
		while (op != 0);
		
	}

	private static int mostrarMenu() {
		System.out.println();
		System.out.println("-------------------- Mastermind -------------------");
		System.out.println("1.- Generar combinació");
		System.out.println("2.- Jugar partida");
		System.out.println("0.- Sortir");
		System.out.println("------------------ -------------- -----------------");
		System.out.println();
		
		int op = llegirNum("Escull una opció: ", "Error, opció invàlida.");
		
		return op;
	}
	
	static int llegirNum(String missatge1, String missatge2) {
		int num = 0;
		boolean correcte = false;
		
		do {
			System.out.println();
			System.out.println(missatge1);

			try {
				num = src.nextInt();
				correcte = true;
			}
			
			catch (Exception e){
				System.out.println(missatge2);
				src.nextLine();
			}
		}
		
		while (correcte == false);
		
		return num;
	}
}