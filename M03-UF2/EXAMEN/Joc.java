package EXAMEN;

import java.util.Scanner;
import java.util.Random;

public class Joc {

    static Scanner src = new Scanner(System.in);
    static Random rnd = new Random();

    public static void jugarPartida(int[] combinacio) {
        boolean acabat = false;
        
        

        do {
            int op = Programa.llegirNum("Vols seguir jugant (1) o sortir (0)? ", "Error, introdueix un nombre vàlid entre 1 i 0");

            switch (op) {
                case 1:
                    

                    if (acabat) {
                        System.out.println();
                        System.out.println("FELICITATS, has guanyat!");
                    }

                    break;

                case 0:
                    acabat = true;
                    System.out.println();
                    System.out.println("Fi del Joc");
                    break;

                default:
                    System.out.println("Error, introdueix un valor correcte");
                    System.out.println();
            }
        } while (!acabat);
    }

    private static void omplirTauler(char[][] taula, int files, int columnes) {
        for (int i = 0; i < files; i++) {
            for (int j = 0; j < columnes; j++) {
                taula[i][j] = '□';
            }
        }
    }

    private static void mostrarTauler(char[][] tauler, char[][] taulerResolt, int files, int columnes) {
        System.out.println();
        System.out.println("--------------- ESTAT DE LA PARTIDA ---------------");
        System.out.println();
        System.out.println();

        for (int i = 0; i < files; i++) {
            for (int j = 0; j < columnes; j++) {
                System.out.printf("%-3s", tauler[i][j]);
            }
            System.out.println();
        }

        System.out.println();
        System.out.println();
        System.out.println("--------------- ------------------- ---------------");
        System.out.println();

    }

}
