package recurs;
import java.util.Scanner;

public class recursivitat1 {
		
		// 1
	    public static long calcularFactorial(int n) {
	        if (n == 0 || n == 1) {
	            return 1;
	        } 
	        
	        else {
	            return n * calcularFactorial(n - 1);
	        }
	        
	    }

	    // 2
	    public static double aproximarE(int n) {
	        if (n == 0) {
	            return 1.0;
	        } 
	        
	        else {
	            return 1.0 / calcularFactorial(n) + aproximarE(n - 1);
	        }
	        
	    }

	    // 3
	    public static int dividirRestesSuccessives(int dividend, int divisor) {
	        if (dividend < divisor) {
	            return 0;
	        } 
	        
	        else {
	            return 1 + dividirRestesSuccessives(dividend - divisor, divisor);
	        }
	        
	    }

	    // 4
	    public static int suma100Parells(int n) {
	        if (n == 200) {
	            return 0;
	        } 
	        
	        else {
	            return n + suma100Parells(n + 2);
	        }
	        
	    }

	    // 5
	    public static int sumaDigits(int n) {
	        if (n < 10) {
	            return n;
	        } 
	        
	        else {
	            return n % 10 + sumaDigits(n / 10);
	        }
	        
	    }

	    // 6
	    public static int sumaVector(int[] vector, int index) {
	        if (index == vector.length) {
	            return 0;
	        } 
	        
	        else {
	            return vector[index] + sumaVector(vector, index + 1);
	        }
	        
	    }

	    // 7
	    public static double calcularExponent(int n, int x) {
	        if (x == 0) {
	            return 1.0;
	        } 
	        
	        else {
	            return n * calcularExponent(n, x - 1);
	        }
	        
	    }

	    // 8
	    public static int calcularMCD(int a, int b) {
	        if (b == 0) {
	            return a;
	        } 
	        
	        else {
	            return calcularMCD(b, a % b);
	        }
	        
	    }

	    // 9
	    public static int calcularProducteRus(int n, int m) {
	        if (n == 1) {
	            return m;
	        } 
	        
	        else if (n % 2 == 0) {
	            return calcularProducteRus(n / 2, m * 2);
	        } 
	        
	        else {
	            return m + calcularProducteRus((n - 1) / 2, m * 2);
	        }
	        
	    }

	    // 10
	    public static int binariADecimal(int binary) {
	        if (binary == 0) {
	            return 0;
	        } else {
	            return binary % 10 + 2 * binariADecimal(binary / 10);
	        }
	    }

	    // 11
	    public static String decimalAOctal(int decimal) {
	        if (decimal == 0) {
	            return "0";
	        } 
	        
	        else {
	            return decimalAOctalRecursiu(decimal, "");
	        }
	    }

	    private static String decimalAOctalRecursiu(int decimal, String result) {
	        if (decimal == 0) {
	            return result;
	        } 
	        
	        else {
	            int remainder = decimal % 8;
	            result = remainder + result;
	            return decimalAOctalRecursiu(decimal / 8, result);
	        }
	    }
}

