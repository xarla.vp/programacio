package MastermindSenseJUnit;

import java.util.Scanner;

/** Classe a on es desenvolupa el funcionament principal del joc 
 * - El métode de jugar rebrà el jugador actual i li demanarà una sèrie d'instruccions per poder manipular una de les caselles disponibles
 * - Al final comprovarà si queden caselles o ha explotat i dirà si guanya **/

public class Joc {

	static Scanner src = new Scanner(System.in);

	public static void jugarPartida(int[] combinacio) {
		
		// VARIABLES NECESSÀRIES
				boolean guanyador = false;										// Defineix si l'usuari ha guanyat o no
					
				boolean acabat = false;
				
				int[] vecJugador = new int [4];
				
				char[] encertades = {' ', ' ', ' ', ' '};
				
				int contCasella = 0;
				
				do {
					int op = Programa.llegirNum("Vols seguir jugant (1) o sortir (0)? ", "Error, introdueix un nombre vàlid entre 1 i 0");
					
					// COMENÇA LA PARTIDA
					switch (op) {
						case 1:
							
	
					            mostrarVector(combinacio, vecJugador, encertades);
					            		   
					            contCasella = 1;
					            vecJugador[0] = demanarNumero(contCasella);
					            
					            contCasella++;
					            vecJugador[1] = demanarNumero(contCasella);
					            
					            contCasella++;
					            vecJugador[2] = demanarNumero(contCasella);
					            
					            contCasella++;
					            vecJugador[3] = demanarNumero(contCasella);
					            
					            encertades = casellaCorrecta(vecJugador, combinacio);
					            
					            acabat = comprovarEncertades(encertades);
					            
					            if (acabat) {
									System.out.println();
						        	System.out.println("FELICITATS, has guanyat!");
						        }
					            
						break;
						
						case 0:
							System.out.println();
							System.out.println("Fi del Joc");
						break;
					
						default: 
							System.out.println("Error, introdueix un valor correcte");
							System.out.println();
							
				    }
				} while (acabat == false);
				
			}

			private static boolean comprovarEncertades(char[] encertades) {
				
				boolean acabat = true;
				
				for (int i = 0; i < encertades.length; i++) {
					if (encertades[i] != '*') {
						acabat = false;
					}
				}
				
				return acabat;
			}

			private static char[] casellaCorrecta(int[] vecJugador, int[] combinacio) {
				
				// Reiniciem les encertades
				
				char[] encertades = {' ', ' ', ' ', ' '}; 

				// Comprovem si hi han encertades a la resta de nombres 
				
				for (int i = 0; i < vecJugador.length; i++) {
					for (int j = 0; j < combinacio.length; j++){
						if (vecJugador[i] == combinacio[j]) {
							encertades[i] = '?';
						}
					}
				}	
				
				// Comprovem si n'hi han plens
				
				for (int i = 0; i < vecJugador.length; i++) {
					if (vecJugador[i] == combinacio[i]) {
						encertades[i] = '*';
					}
				}						
				
				return encertades;
			}
			
			private static int demanarNumero(int contCasella) {
				int num = 0;
				
				do {
					num = Programa.llegirNum("Digues un número del 1 al 9 per a la " + contCasella + "a casella : ", "Error, segueix les instruccions ");
				} while(num < 0 || num > 9);
				
				return num;
			}

			private static void mostrarVector(int[] combinacio, int[] vecJugador, char[] encertades) {
				System.out.println();
				System.out.println("--------------- ESTAT DE LA PARTIDA ---------------");
				System.out.println();
				System.out.println();
				
				System.out.println("ENCERTADES = '*'");
				System.out.println("EN EL TAULER = '?'");
				
				System.out.println();
				System.out.println();
				
				for (int i = 0; i < vecJugador.length; i++) {
					System.out.print(vecJugador[i] + " ");
				}
				
				System.out.println();

				for (int i = 0; i < encertades.length; i++) {
					System.out.print(encertades[i] + " ");
				}

				System.out.println();				
			}
			
}
			