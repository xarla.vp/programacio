package MastermindSenseJUnit;

import java.util.Random;
import java.util.Scanner;

/** Classe que llança l'aplicació, funciona com a distribuidor de tasques mitjançant un menú d'opcions **/

public class Programa {

	/** 1.- Generar combinació
		2.- Jugar partida
		0.- Sortir			**/
	static Scanner src = new Scanner(System.in);
	static Random rnd = new Random();
	
	public static void main(String[] args) {
		int op = 0; // Desa l'opció escollida per l'usuari
		int[] combinacio = new int[4]; // Crea un vector de 4 nombres
		
		boolean generada = false; // Desa si hi ha una generació nova o no
		
		do {
			op = mostrarMenu();
			
				switch (op) {
				
				case 1: 
					combinacio = generarCombinacio();
					generada = true;
				break;
				
				case 2: 
					if (generada) { // Si no hi ha cap generació nova, no es pot jugar
	                    Joc.jugarPartida(combinacio);
	                } 
					else {
						System.out.println();
	                    System.out.println("Error, has de generar un tauler abans de començar.");
	                }
					
	            break;
				
				case 0: 
					System.out.println();
					System.out.println("Fi del Joc");
				break;
				
				default: 
					System.out.println();
					System.out.println("Error, introdueix un valor correcte");
			}
		}
		
		while (op != 0);
		
	}

	private static int[] generarCombinacio() {
		int [] comb = new int [4];
		
		comb[0] = rnd.nextInt(9);
		
		do {
			comb[1] = rnd.nextInt(9);
		} while (comb[1] == comb[0]);
		
		do {
			comb[2] = rnd.nextInt(9);
		} while (comb[2] == comb[0] || comb[2] == comb[1]);
		
		do {
			comb[3] = rnd.nextInt(9);
		} while (comb[3] == comb[0] || comb[3] == comb[1] || comb[3] == comb[2]);
		
		return comb;
	}

	private static int mostrarMenu() {
		System.out.println();
		System.out.println("-------------------- Mastermind -------------------");
		System.out.println("1.- Generar combinació");
		System.out.println("2.- Jugar partida");
		System.out.println("0.- Sortir");
		System.out.println("------------------ -------------- -----------------");
		System.out.println();
		
		int op = llegirNum("Escull una opció: ", "Error, opció invàlida.");
		
		return op;
	}
	
	static int llegirNum(String missatge1, String missatge2) {
		int num = 0;
		boolean correcte = false;
		
		do {
			System.out.println();
			System.out.println(missatge1);

			try {
				num = src.nextInt();
				correcte = true;
			}
			
			catch (Exception e){
				System.out.println(missatge2);
				src.nextLine();
			}
		}
		
		while (correcte == false);
		
		return num;
	}
}