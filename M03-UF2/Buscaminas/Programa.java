package Buscaminas;

import java.util.ArrayList;
import java.util.Scanner;

/** Classe que llança l'aplicació, funciona com a distribuidor de tasques mitjançant un menú d'opcions **/

public class Programa {

	/** 1.- Mostrar Ajuda
		2.- Definir Jugadors
		3.- Jugar Partida
		4.- Veure Jugadors
		0.- Sortir			**/
	static Scanner src = new Scanner(System.in);
	
	public static void main(String[] args) {
		int op = 0; // Desa l'opció escollida per l'usuari
		String jugador1 = "";
		int[] dificultat = {8, 8, 10};
		boolean resultat = false;
		
		boolean definit = false;
		
		do {
			op = mostrarMenu();
			
				switch (op) {
				case 1: 
					Ajuda.mostrarAjuda();
				break;
				
				case 2: 
					jugador1 = Jugador.definirJugador();
					definit = true;
				break;
				
				case 3: 
					dificultat = escollirDificultat(dificultat);
				break;
				
				case 4: 
					if (definit) {
	                    resultat = Joc.jugarPartida(jugador1, dificultat);
	                    Jugador.actualitzarResultat(jugador1, resultat);
	                    definit = false;
	                } 
					else {
	                    System.out.println("Error, has de definir els jugadors abans de començar.");
	                }
	            break;
				
				case 5: 
					Jugador.veureResultat(jugador1);
				break;
				
				case 0: 
					System.out.println();
					System.out.println("Fi del Joc");
				break;
				
				default: 
					System.out.println("Error, introdueix un valor correcte");
					System.out.println();
			}
		}
		
		while (op != 0);
		
	}

	private static int mostrarMenu() {
		System.out.println();
		System.out.println("------------------- Buscaminas ---------------------");
		System.out.println("1.- Mostrar ajuda");
		System.out.println("2.- Definir jugadors");
		System.out.println("3.- Definir dificultat");
		System.out.println("4.- Jugar partida");
		System.out.println("5.- Resultats jugador");
		System.out.println("0.- Sortir");
		System.out.println("------------------ -------------- -----------------");
		System.out.println();
		
		boolean correcte = false;
		
		int op = llegirNum("Escull una opció: ", "Error, opció invàlida.");
		
		return op;
	}
	
	public static int[] escollirDificultat(int[] dificultat) {
		int op = 0;
				
		do {
			op = mostrarMenuDificultats();
			
				switch (op) {
				case 1: 
					dificultat[0] = 8; 		// files
					dificultat[1] = 8; 		// columnes
					dificultat[2] = 10; 	// mines
					
				break;
				
				case 2: 
					dificultat[0] = 16; 	// files
					dificultat[1] = 16; 	// columnes
					dificultat[2] = 40; 	// mines
					
				break;
				
				case 3: 
					dificultat[0] = 16; 	// files
					dificultat[1] = 30; 	// columnes
					dificultat[2] = 99; 	// mines
					
				break;
				
				case 4: 
					
					do {
						dificultat[0] = llegirNum("Insereix un nombre de files vàlid (de 8 a 24): ", "Error, segueix les instruccions.");
						System.out.println();
					}
					
					while (dificultat[0] < 8 || dificultat[0] > 24);
					
					do {
						dificultat[1] = llegirNum("Insereix el nombre de columnes vàlid (de 8 a 32): ", "Error, segueix les instruccions.");
						System.out.println();
					}
					
					while (dificultat[1] < 8 || dificultat[1] > 32);
					
					int mines = (dificultat[0] * dificultat[1]) / 3;

					do {
						dificultat[2] = llegirNum("Insereix un nombre de mines vàlid (un màxim de " + mines + " mines): ", "Error, segueix les instruccions.");
						System.out.println();
					}
					
					while (mines < dificultat[2]);
					
	            break;
	            
				case 0: 
					System.out.println();
					System.out.println("Fi del Joc");
				break;
				
				default: 
					System.out.println("Error, introdueix un valor correcte");
					System.out.println();
			}
		}
		
		while (op != 0);
		
		return dificultat;
	}
	
	static int llegirNum(String missatge1, String missatge2) {
		int num = 0;
		boolean correcte = false;
		
		do {
			System.out.println();
			System.out.println(missatge1);

			try {
				num = src.nextInt();
				correcte = true;
			}
			
			catch (Exception e){
				System.out.println(missatge2);
				src.nextLine();
			}
		}
		
		while (correcte == false);
		
		return num;
	}

	private static int mostrarMenuDificultats() {
		System.out.println();
		System.out.println("------------------- Dificultats --------------------");
		System.out.println("1.- Principiant");
		System.out.println("2.- Intermedi");
		System.out.println("3.- Expert");
		System.out.println("4.- Personalitzat");
		System.out.println("0.- Sortir");
		System.out.println("------------------ -------------- -----------------");
		System.out.println();
		
		int op = llegirNum("Escull una opció: ", "Error, opció invàlida.");
		
		return op;
	}

}
