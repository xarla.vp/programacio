package Buscaminas;

import java.util.Hashtable;
import java.util.Scanner;

/** Classe que gestiona la informació sobre els jugadors i fa:
 * - Alta de Jugadors
 * - Baixa de Jugadors
 * - Actualitzar partides guanyades
 * - Consultar partides guanyades**/

public class Jugador {
	
	static Hashtable<String, Integer> jugadors = new Hashtable<String, Integer> ();
	static Scanner src = new Scanner(System.in);
	
	public static String definirJugador() {
		String nom;
		System.out.println("Indica el teu nom: ");
		nom = src.nextLine();
		
		if (!jugadors.containsKey(nom)) {
			jugadors.put(nom, 0);
		}
		
		return nom;
	}

	public static void actualitzarResultat(String jugador1, boolean resultat) { // El resultat determina quin dels dos ha guanyat (1 o 2)
		if (jugadors.containsKey(jugador1) && resultat == true) {
			jugadors.put(jugador1, jugadors.get(jugador1) + 1);
		}
	}

	public static void veureResultat(String jugador1) {
		if (jugadors.containsKey(jugador1)) {
			System.out.println();
			System.out.println("Partides guanyades per " + jugador1 + " : " + jugadors.get(jugador1));
		}
	}

	public static void veureRanking() {
		for (String key : jugadors.keySet()) {
			System.out.println(key + " ==> " + jugadors.get(key));
		}
	}

}
