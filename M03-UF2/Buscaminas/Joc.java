package Buscaminas;

import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

/** Classe a on es desenvolupa el funcionament principal del joc 
 * - El métode de jugar rebrà el jugador actual i li demanarà una sèrie d'instruccions per poder manipular una de les caselles disponibles
 * - Al final comprovarà si queden caselles o ha explotat i dirà si guanya **/

public class Joc {

	static Scanner src = new Scanner(System.in);
	static Random rnd = new Random();

	public static boolean jugarPartida(String jugador1, int[] dificultat) {
		
		// VARIABLES NECESSÀRIES
				boolean guanyador = false;										// Defineix si l'usuari ha guanyat o no
					
				boolean acabat = false;											// Defineix si la partida ha finalitzat o no
				
				int files = dificultat[0] + 2;
				int columnes = dificultat[1] + 2;
				
				int contBombes = dificultat[2];
				int bombesTauler = contBombes;
				
				int contMoviments = 0;
				
				char[][] tauler = new char[files][columnes];					// Crea un tauler buit a l'inici del programa
				char[][] taulerResolt = new char[files][columnes];				// Crea un tauler amb bombes aleatories i els seus respectius nombres al voltant
				
				omplirTauler(tauler, files, columnes);
				omplirTauler(taulerResolt, files, columnes);
				
				taulerResolt = generarTaulerResolt(taulerResolt, files, columnes, bombesTauler);
				
				int fil = 0;
				int col = 0; // Fila i columna de la casella de l'usuari
				
				// COMENÇA LA PARTIDA
								
				do {

		            mostrarTauler(tauler, taulerResolt, jugador1, columnes, files, contMoviments);

		            contMoviments = picarCasella(tauler, taulerResolt, fil, col, contMoviments, files, columnes, contBombes, acabat); // L'Usuari defineix una casella a picar

		            acabat = tipusCasella(tauler, taulerResolt, fil, col);

		            acabat = quedenBombes(tauler, taulerResolt, acabat, columnes, files);

		        } while (acabat == false);

		        guanyador = comprovarGuanyador(tauler, taulerResolt, acabat, jugador1, guanyador, files, columnes);

		        return guanyador;

		    }

		    private static boolean comprovarGuanyador(char[][] tauler, char[][] taulerResolt, boolean acabat, String jugador1,
		            boolean guanyador, int files, int columnes) {
		        for (int i = 0; i < tauler.length; i++) {
		            for (int j = 0; j < tauler[0].length; j++) {

		                if (tauler[i][j] == '□' && taulerResolt[i][j] != 'X') {
		                    return false;
		                }

		                if (tauler[i][j] == '■' && taulerResolt[i][j] != 'X') {
		                    return false;
		                }
		            }
		        }
		        return true;
		    }

		    private static char[][] generarTaulerResolt(char[][] taulerResolt, int files, int columnes, int bombesTauler) {
		        int i = 0;
		        int j = 0;

		        while (bombesTauler > 0) {

		            do {
		                i = rnd.nextInt(files - 1);
		                j = rnd.nextInt(columnes - 1);
		            } while (taulerResolt[i][j] != '□');

		            taulerResolt[i][j] = 'X';

		            bombesTauler--;

		        }

		        for (i = 1; i < files - 1; i++) {
		            for (j = 1; j < columnes - 1; j++) {

		                int cont = 0;

		                if (taulerResolt[i][j] == 'X') {

		                }

		                else {
		                    if (taulerResolt[i - 1][j - 1] == 'X') {
		                        cont++;
		                    }

		                    if (taulerResolt[i - 1][j] == 'X') {
		                        cont++;
		                    }

		                    if (taulerResolt[i - 1][j + 1] == 'X') {
		                        cont++;
		                    }

		                    if (taulerResolt[i][j - 1] == 'X') {
		                        cont++;
		                    }

		                    if (taulerResolt[i][j + 1] == 'X') {
		                        cont++;
		                    }

		                    if (taulerResolt[i + 1][j - 1] == 'X') {
		                        cont++;
		                    }

		                    if (taulerResolt[i + 1][j] == 'X') {
		                        cont++;
		                    }

		                    if (taulerResolt[i + 1][j + 1] == 'X') {
		                        cont++;
		                    }

		                    if (cont > 0) {
		                        taulerResolt[i][j] = Character.forDigit(cont, 10);
		                    }

		                }
		            }
		        }

		        return taulerResolt;
		    }

		    private static boolean quedenBombes(char[][] tauler, char[][] taulerResolt, boolean acabat, int columnes, int files) {
		        for (int i = 0; i < tauler.length; i++) {
		            for (int j = 0; j < tauler[0].length; j++) {
		                if (tauler[i][j] == '□') {
		                    return false;
		                }
		            }
		        }
		        return true;
		    }

		    private static boolean tipusCasella(char[][] tauler, char[][] taulerResolt, int fil, int col) {
		        boolean acabat = false;

		        if (taulerResolt[fil][col] == 'X') {
		            acabat = true;
		        }

		        else if (taulerResolt[fil][col] != '□') {
		            tauler[fil][col] = taulerResolt[fil][col];
		        }

		        else if (taulerResolt[fil][col] == '□') {
		            tauler = desvelarCasellesProperes(fil, col, tauler, taulerResolt);// hacer que peten todas las vacías adyacentes a esta
		        }

		        return acabat;
		    }

		    private static char[][] desvelarCasellesProperes(int fil, int col, char[][] tauler, char[][] taulerResolt) {
		        if (taulerResolt[fil][col] == '□') {
		            if (tauler[fil][col] != '*') {
		                tauler[fil][col] = '*';
		                taulerResolt[fil][col] = '*';

		                tauler = desvelarCasellesProperes(fil - 1, col - 1, tauler, taulerResolt);
		                tauler = desvelarCasellesProperes(fil - 1, col, tauler, taulerResolt);
		                tauler = desvelarCasellesProperes(fil - 1, col + 1, tauler, taulerResolt);
		                tauler = desvelarCasellesProperes(fil, col - 1, tauler, taulerResolt);
		                tauler = desvelarCasellesProperes(fil, col + 1, tauler, taulerResolt);
		                tauler = desvelarCasellesProperes(fil + 1, col - 1, tauler, taulerResolt);
		                tauler = desvelarCasellesProperes(fil + 1, col, tauler, taulerResolt);
		                tauler = desvelarCasellesProperes(fil + 1, col + 1, tauler, taulerResolt);
		            }
		        }

		        else {
		            tauler[fil][col] = taulerResolt[fil][col];
		        }

		        return tauler;
		    }
		    
			private static int picarCasella(char[][] tauler, char[][] taulerResolt, int fil, int col, int contMoviments, int files, int columnes, int bombesTauler, boolean acabat) {
			    System.out.println("Vols marcar/desmarcar una bomba o picar una casella? ");
			    System.out.println("1.- Marcar");
			    System.out.println("2.- Desmarcar");
			    System.out.println("3.- Picar");

			    int op = Programa.llegirNum("Escull una de les opcions: ", "Error, has d'escollir entre '1', '2' i '3'");
			    System.out.println();

			    System.out.println("Escull un casella");
			    
			    fil = 0;
			    col = 0;
			    
			    do {
			    	fil = Programa.llegirNum("(fila): ", "Error, nombre invàlid");
			    }
			    
			    while (fil < 1 || fil > files - 2);
			    
			    do {
				    col = Programa.llegirNum("(columna): ", "Error, nombre invàlid");
			    }
			    
			    while (col < 1 || col > columnes - 2);
			    
			    switch (op) {
			        case 1:

			            do {

			                System.out.println();

			                if (tauler[fil][col] == '□') {
			                    tauler[fil][col] = '■';
			                    contMoviments++;
			                    if (contMoviments == bombesTauler) {
			                        System.out.println("Has trobat totes les bombes! Has guanyat!");
			                        acabat = true;
			                        return contMoviments;
			                    }
			                } 
			                
			                else {
			                    System.out.println("Escull un casella vàlida (ha de ser un '□'): ");
			                    do {
			    			    	fil = Programa.llegirNum("(fila): ", "Error, nombre invàlid");
			    			    }
			    			    
			    			    while (fil < 1 || fil > files - 2);
			    			    
			    			    do {
			    				    col = Programa.llegirNum("(columna): ", "Error, nombre invàlid");
			    			    }
			    			    
			    			    while (col < 1 || col > columnes - 2);
			                }

			            } while (tauler[fil][col] != '□');
			            
			            contMoviments++;

			            break;

			        case 2:

			            do {

			                System.out.println();

			                if (tauler[fil][col] == '■') {
			                    tauler[fil][col] = '□';
			                } else {
			                    System.out.println("Escull un casella vàlida (ha de ser un '■'): ");
			                    do {
			    			    	fil = Programa.llegirNum("(fila): ", "Error, nombre invàlid");
			    			    }
			    			    
			    			    while (fil < 1 || fil > files - 2);
			    			    
			    			    do {
			    				    col = Programa.llegirNum("(columna): ", "Error, nombre invàlid");
			    			    }
			    			    
			    			    while (col < 1 || col > columnes - 2);
			                }

			            } while (tauler[fil][col] != '■');
			            
			            contMoviments++;

			            break;

			        case 3:

			            System.out.println();

			            if (taulerResolt[fil][col] != '□' && contMoviments == 0) {
			                do {
			                    omplirTauler(taulerResolt, bombesTauler, bombesTauler);
			                    taulerResolt = generarTaulerResolt(taulerResolt, files, columnes, bombesTauler);
			                } while (taulerResolt[fil][col] != '□' && contMoviments == 0); // la primera casella ha de ser un espai blanc

			            }
			            
			            tauler = desvelarCasellesProperes(fil, col, tauler, taulerResolt);
			            
			            contMoviments++;

			            break;

			        default:
			            System.out.println("Error, introdueix un valor correcte");
			            System.out.println();
			    }

			    return contMoviments;
			}
			
			private static void omplirTauler(char[][] taula, int files, int columnes) {
			    for (int i = 0; i < files; i++) {
			        for (int j = 0; j < columnes; j++) {
			            if (i == 0 || j == 0 || i == files - 1 || j == columnes - 1) {
			                taula[i][j] = ' ';
			            } else {
			                taula[i][j] = '□'; // □ ■
			            }
			        }
			    }
			    
			}


			private static void mostrarTauler(char[][] tauler, char[][] taulerResolt, String jugador1, int files, int columnes, int contMoviments) {
				System.out.println();
				System.out.println("--------------- ESTAT DE LA PARTIDA ---------------");System.out.println();
				System.out.println();
				
				System.out.println("MOVIMENTS: " + contMoviments);
				System.out.println();
				
				System.out.println("TAULER: ");
				System.out.println();

				for (int i = 0; i < files; i++) {
				    for (int j = 0; j < columnes; j++) {
				    	System.out.printf("%-3s", taulerResolt[i][j]);
				   }
				    System.out.println();
				}

				
				System.out.println();
				System.out.println();
				for (int i = 0; i < files; i++) {
				    for (int j = 0; j < columnes; j++) {
				    	System.out.printf("%-3s", tauler[i][j]);
				    }
				    System.out.println();
				}
				
				System.out.println();
				System.out.println("--------------- ------------------- ---------------");
				System.out.println();
			}
			
}
			